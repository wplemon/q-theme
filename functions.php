<?php
/**
 * Q Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Q Theme
 */

use Q_Theme\Admin;
use Q_Theme\EDD;
use Q_Theme\AMP;
use Q_Theme\Widget_Output_Filters;

/**
 * Require the main theme class.
 *
 * @since 1.0
 */
require_once get_template_directory() . '/inc/q-theme.php';

/**
 * The Q-Theme Autoloader.
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(
	function( $class ) {
		$name = strtolower( str_replace( '_', '-', substr( $class, 7 ) ) );
		if ( 0 === strpos( $class, 'Q_Theme' ) ) {
			$path = __DIR__ . '/inc' . str_replace( '\\', '/', "$name/$name" ) . '.php';
			if ( file_exists( $path ) ) {
				require_once $path;
			}
			$path = __DIR__ . '/inc' . str_replace( '\\', '/', $name ) . '.php';
			if ( file_exists( $path ) ) {
				require_once $path;
			}
		}
	}
);

// Add the widget filters.
Widget_Output_Filters::get_instance();

if ( ! class_exists( 'EDD_Theme_Updater_Admin' ) ) {
	require_once __DIR__ . '/inc/theme-updater-admin.php';
}

/**
 * The theme version.
 *
 * @since 1.0
 */
define( 'Q_THEME_VERSION', '1.0' );

/**
 * Whether we want to beta-test new versions or not.
 * Can be overriden in wp-config.php.
 *
 * @since 1.0
 */
if ( ! defined( 'Q_THEME_BETA' ) ) {
	define( 'Q_THEME_BETA', true );
}

/**
 * Load the textdomain.
 *
 * @since 1.0
 */
function q_theme_load_theme_textdomain() {
	load_theme_textdomain( 'q-theme', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'q_theme_load_theme_textdomain' );

/**
 * Load admin screen.
 *
 * @since 1.0
 */
new Admin();

/**
 * Load the main theme class.
 *
 * @since 1.0
 * @return Q_Theme
 */
function q_theme() {
	return Q_Theme::get_instance();
}

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customize.php';

/**
 * Load EDD mods.
 *
 * @since 1.0
 */
if ( class_exists( 'Easy_Digital_Downloads' ) ) {
	new EDD();
}

/**
 * Post-edit link.
 *
 * @since 1.0
 * @return void
 */
function q_theme_the_edit_link() {
	edit_post_link(
		sprintf(
			/* translators: %s: Name of the post.*/
			esc_html__( 'Edit %s', 'q-theme' ),
			'<span class="screen-reader-text">' . get_the_title() . '</span>'
		),
		'<span class="edit-link">',
		'</span>'
	);
}

/**
 * Comments link.
 *
 * @since 1.0
 * @return void
 */
function q_theme_the_comments_link() {
	comments_popup_link(
		sprintf(
			/* translators: %s: post title */
			'<span class="screen-reader-text">' . esc_html__( 'Leave a Comment on %s', 'q-theme' ) . '</span>',
			get_the_title()
		)
	);
}

/**
 * Adds support for wp.com-specific theme functions.
 *
 * @global array $themecolors
 */
function q_theme_wpcom_setup() {
	global $themecolors;

	// Set theme colors for third party services.
	if ( ! isset( $themecolors ) ) {
		$themecolors = [
			'bg'     => '',
			'border' => '',
			'text'   => '',
			'link'   => '',
			'url'    => '',
		];
	}
}
add_action( 'after_setup_theme', 'q_theme_wpcom_setup' );

/**
 * Move categories counts inside the links.
 *
 * @since 1.0
 * @param string $html The links.
 * @return string
 */
function q_theme_filter_wp_list_categories( $html ) {
	return str_replace(
		[ '</a> (', ')' ],
		[ ' (', ')</a>' ],
		$html
	);
}
add_filter( 'wp_list_categories', 'q_theme_filter_wp_list_categories' );

/**
 * Add classes to the comment-form submit button.
 *
 * @since 1.0
 * @param array $args The comment-form args.
 * @return array
 */
function q_theme_filter_comment_form_defaults( $args ) {
	$args['class_submit'] .= ' wp-block-button__link';
	return $args;
}

/**
 * Returns an array of the singular parts for post, pages & CPTs.
 *
 * @since 1.0
 * @return array
 */
function q_theme_get_post_parts() {
	return apply_filters(
		'q_theme_get_post_parts',
		get_theme_mod( 'q_theme_post_parts', [ 'post-title', 'post-date-author', 'post-thumbnail', 'post-content', 'post-category', 'post-tags', 'post-comments-link' ] )
	);
}

/**
 * Integrates WPBakery Builder in the theme.
 *
 * @since 1.0
 */
if ( function_exists( 'vc_set_as_theme' ) ) {
	add_action( 'vc_before_init', 'vc_set_as_theme' );
}

/**
 * Remove logo.
 *
 * @since 1.0
 */
add_filter( 'vc_nav_front_logo', '__return_empty_string' );

/**
 * Get an array of settings we don't want to save in templates.
 *
 * @return array
 */
function q_theme_get_template_theme_mods_blacklist() {
	$blacklist = [
		'nav_menu_locations',
		'nav_menu',
		'add_menu',
		'nav_menu_item',
		'q_theme_logo_focus_on_core_section',
		'q_theme_footer_focus_on_core_widgets_section',
		'q_theme_logo_focus_on_typography_section',
		'q_theme_logo_focus_on_sidebar-1_section',
		'q_theme_logo_focus_on_sidebar-2_section',
		'q_theme_logo_focus_on_sidebar-3_section',
		'q_theme_logo_focus_on_sidebar-4_section',
		'q_theme_logo_focus_on_sidebar-5_section',
		'q_theme_logo_focus_on_sidebar-6_section',
		'q_theme_logo_focus_on_sidebar-7_section',
		'q_theme_logo_focus_on_sidebar-8_section',
		'q_theme_logo_focus_on_sidebar-9_section',
		'q_theme_logo_focus_on_sidebar-10_section',
	];
	return apply_filters( 'q_theme_template_blacklist_settings', $blacklist );
}

/**
 * TGMPA
 *
 * @since 1.0
 */
require_once get_parent_theme_file_path( '/inc/tgmpa-config.php' );

/**
 * Merlin Onboarding.
 *
 * @since 1.0
 */
require_once get_parent_theme_file_path( '/inc/merlin/vendor/autoload.php' );
require_once get_parent_theme_file_path( '/inc/merlin/class-merlin.php' );
require_once get_parent_theme_file_path( '/inc/merlin-config.php' );

// Init AMP Support.
new AMP();

/**
 * Generates the HTML for a toggle button.
 * 
 * @param array $args The button arguments.
 * @since 1.0
 */
function q_theme_toggle_button( $args ) {

	$html = '';

	if ( AMP::is_active() ) {

		// Create new state for managing storing the whether the sub-menu is expanded.
		$html .= '<amp-state id="' . esc_attr( $args['expanded_state_id'] ) . '">';
		$html .= '<script type="application/json">' . $args['expanded'] . '</script>';
		$html .= '</amp-state>';
	}

	if ( ! isset( $args['classes'] ) ) {
		$args['classes'] = [];
	}
	$args['classes'][] = 'q-toggle';
	$classes           = implode( ' ', array_unique( $args['classes'] ) );

	$button_atts = [
		'aria-expanded' => 'false',
	];

	if ( AMP::is_active() ) {
		$button_atts['[class]']         = $classes . '+(' . $args['expanded_state_id'] . '?\'toggled-on\':\'\')';
		$button_atts['[aria-expanded]'] = "{$args['expanded_state_id']} ? 'true' : 'false'";
		$button_atts['on']              = "tap:AMP.setState({ {$args['expanded_state_id']}: ! {$args['expanded_state_id']} })";
	}

	/*
	 * Create the toggle button which mutates the state and which has class and
	 * aria-expanded attributes which react to the state changes.
	 */
	$html .= '<button class="' . $classes . '"';
	foreach ( $button_atts as $key => $val ) {
		if ( ! empty( $key ) ) {
			$html .= ' ' . $key . '="' . $val . '"';
		}
	}
	$html .= '>';

	if ( AMP::is_active() && isset( $args['screen_reader_label_collapse'] ) && isset( $args['screen_reader_label_expand'] ) ) {
		
		// Let the screen reader text in the button also update based on the expanded state.
		$html .= '<span class="screen-reader-text"';
		$html .= ' [text]="' . $args['expanded_state_id'] . '?\'' . esc_attr( $args['screen_reader_label_collapse'] ) . '\':\'' . esc_attr( $args['screen_reader_label_expand'] ) . '\'">';
		$html .= esc_html( $args['screen_reader_label_expand'] );
	} elseif ( isset( $args['screen_reader_label_toggle'] ) ) {
		$html .= '<span class="screen-reader-text">' . $args['screen_reader_label_toggle'] . '</span>';
	}
	$html .= '</span>';
	$html .= $args['label'];
	$html .= '</button>';

	return $html;
}

add_filter( 'wp_nav_menu_args', function( $args ) {
	$args['menu_class'] .= ' q-navigation';
	return $args;
} );
