<?php
/**
 * Q Theme Customizer
 *
 * @package Q Theme
 */

use Q_Theme\Customizer;
use Q_Theme\Customizer\Template;
use Q_Theme\Customizer\Control\Grid;

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function q_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
	$wp_customize->get_setting( 'custom_logo' )->transport     = 'refresh';
}
add_action( 'customize_register', 'q_theme_customize_register' );

add_filter( 'kirki_control_types', function( $controls ) {
	$controls['q_theme_grid']   = 'Q_Theme\Customizer\Control\Grid';
	$controls['q_section_help'] = 'Q_Theme\Customizer\Control\Help';
	return $controls;
} );

require_once get_template_directory() . '/inc/customizer/kirki-wcag-tc/kirki-wcag-tc.php';
require_once get_template_directory() . '/inc/customizer/kirki-wcag-lc/kirki-wcag-lc.php';

/**
 * Modify the URL for assets in our custom control.
 *
 * @return string
 */
function q_theme_kirki_wcag_text_color() {
	return get_template_directory_uri() . '/inc/customizer/kirki-wcag-tc';
}
add_filter( 'kirki_wcag_text_color_url', 'q_theme_kirki_wcag_text_color' );

/**
 * Modify the URL for assets in our custom control.
 *
 * @return string
 */
function q_theme_kirki_wcag_link_color() {
	return get_template_directory_uri() . '/inc/customizer/kirki-wcag-lc';
}
add_filter( 'kirki_wcag_link_color_url', 'q_theme_kirki_wcag_link_color' );


/**
 * Proxy function for Kirki.
 *
 * @since 1.0
 * @param array $args The field arguments.
 * @return void
 */
function q_theme_add_customizer_field( $args ) {
	if ( ! class_exists( 'Kirki' ) ) {
		return;
	}
	Kirki::add_field( 'q-theme', $args );
	if ( 'q_theme_grid' === $args['type'] ) {
		Customizer::$grid_controls[ $args['settings'] ] = $args;
	}
}

/**
 * Proxy function for Kirki.
 *
 * @since 1.0
 * @param string $id   The section ID.
 * @param array  $args The field arguments.
 * @return void
 */
function q_theme_add_customizer_section( $id, $args ) {
	if ( ! class_exists( 'Kirki' ) ) {
		return;
	}
	// WIP: Disable icons.
	if ( isset( $args['icon'] ) ) {
		unset( $args['icon'] );
	}
	Kirki::add_section( $id, $args );
}

/**
 * Proxy function for Kirki.
 *
 * @since 1.0
 * @param string $id   The section ID.
 * @param array  $args The field arguments.
 * @return void
 */
function q_theme_add_customizer_panel( $id, $args ) {
	if ( ! class_exists( 'Kirki' ) ) {
		return;
	}
	// WIP: Disable icons.
	if ( isset( $args['icon'] ) ) {
		unset( $args['icon'] );
	}
	Kirki::add_panel( $id, $args );
}

// Don't continue any further if the Kirki plugin is not installed.
if ( ! class_exists( 'Kirki' ) ) {
	return;
}

q_theme_add_customizer_panel( 'q_theme_options', [
	'title'    => esc_attr__( 'Theme Options', 'q-theme' ),
	'priority' => 1,
] );

/**
 * Rules for a11y controls.
 * These are used in the controls and this is just a shortcut
 * to avoid writing the same code over and over and over and over and over....
 *
 * @since 1.0
 * @return array
 */
function q_theme_a11y_control_rules() {
	return [
		[
			'min'    => 0,
			'max'    => 3,
			'result' => 'error',
			/* translators: the contrast ratio. */
			'text'   => __( '<strong>Contrast with background: %s</strong>. Please choose a color with a contrast greater than 3:1 to meet the minimum WCAG accessibility requirements.', 'q-theme' ),
		],
		[
			'min'    => 3,
			'max'    => 4.5,
			'result' => 'warning',
			/* translators: the contrast ratio. */
			'text'   => __( '<strong>Contrast with background: %s</strong>. Meets WCAG AA requirements <a href="https://www.w3.org/TR/WCAG20/#larger-scaledef" target="_blank">(for bold text or text larger than 24px)</a>.', 'q-theme' ),
		],
		[
			'min'    => 4.5,
			'max'    => 7,
			'result' => 'success',
			/* translators: the contrast ratio. */
			'text'   => __( '<strong>Contrast with background: %s</strong>. Meets WCAG AA requirements for normal text and AAA for large text.', 'q-theme' ),
		],
		[
			'min'    => 7,
			'max'    => 22,
			'result' => 'success',
			/* translators: the contrast ratio. */
			'text'   => __( '<strong>Contrast with background: %s</strong>. Meets WCAG AAA requirements.', 'q-theme' ),
		],
	];
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function q_theme_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function q_theme_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Changes the stylesheet in which Kirki adds its styles.
 *
 * @since 1.0
 * @return string
 */
function q_theme_kirki_stylesheet() {
	return 'q-theme-style';
}
add_filter( 'kirki_q_theme_stylesheet', 'q_theme_kirki_stylesheet' );

/**
 * Adds some custom styles to the customizer.
 *
 * @since 1.0
 */
function q_theme_customizer_custom_styles() {
	echo '<style>';
	echo '#sub-accordion-section-q_theme_to_section .customize-control{margin-bottom:1em;padding-bottom:1em;border-bottom:1px solid rgba(0,0,0,.07);}';
	echo '#customize-controls .control-section.open .control-section-kirki-nested .accordion-section-title{background:#dedede!important;}';
	echo '</style>';
}
add_action( 'customize_controls_print_styles', 'q_theme_customizer_custom_styles', 999 );

if ( class_exists( 'Kirki' ) ) {
	/**
	 * Add the config.
	 */
	Kirki::add_config( 'q-theme', [
		'capability'  => 'edit_theme_options',
		'option_type' => 'theme_mod',
	] );
}

require_once get_template_directory() . '/inc/customizer/section/grid.php';
require_once get_template_directory() . '/inc/customizer/section/typography.php';
require_once get_template_directory() . '/inc/customizer/section/edd.php';
require_once get_template_directory() . '/inc/customizer/section/features.php';
if ( class_exists( 'WooCommerce' ) ) {
	require_once get_template_directory() . '/inc/customizer/section/woocommerce.php';
}

/**
 * Get the default value for the grid.
 *
 * @since 1.0
 * @return array
 */
function q_theme_get_grid_default_value() {
	return [
		'rows'         => 4,
		'columns'      => 2,
		'areas'        => [
			'header'      => [
				'cells' => [ [ 1, 1 ], [ 1, 2 ] ],
			],
			'breadcrumbs' => [
				'cells' => [ [ 2, 1 ] ],
			],
			'content'     => [
				'cells' => [ [ 3, 1 ] ],
			],
			'sidebar_1'   => [
				'cells' => [ [ 2, 2 ], [ 3, 2 ] ],
			],
			'footer'      => [
				'cells' => [ [ 4, 1 ], [ 4, 2 ] ],
			],
		],
		'gridTemplate' => [
			'rows'    => [ 'auto', 'auto', 'auto', 'auto' ],
			'columns' => [ 'auto', '350px' ],
		],
	];
}

/**
 * Get the default value for the grid.
 *
 * @since 1.0
 * @return array
 */
function q_theme_get_grid_mobile_default_value() {
	return [
		'rows'         => 5,
		'columns'      => 1,
		'areas'        => [
			'header'      => [
				'cells' => [ [ 1, 1 ] ],
			],
			'breadcrumbs' => [
				'cells' => [ [ 2, 1 ] ],
			],
			'content'     => [
				'cells' => [ [ 3, 1 ] ],
			],
			'sidebar_1'   => [
				'cells' => [ [ 4, 1 ] ],
			],
			'footer'      => [
				'cells' => [ [ 5, 1 ] ],
			],
		],
		'gridTemplate' => [
			'rows'    => [ 'auto', 'auto', 'auto', 'auto', 'auto' ],
			'columns' => [ 'auto' ],
		],
	];
}

add_action( 'customize_register', function( $wp_customize ) {
	$wp_customize->add_section( 'q_theme_template', [
		'title'    => esc_html__( 'Template', 'q-theme' ),
		'priority' => 2,
	] );

	// Add Template Setting.
	$wp_customize->add_setting( 'q_templates', [
		'default'           => [],
		'transport'         => 'refresh',
		'sanitize_callback' => '__return_true',
	] );
} );

/**
 * Move the background-color control to the modified background-image section.
 *
 * @since 1.0
 * @param WP_Customize The WordPress Customizer main object.
 * @return void
 */
add_action( 'customize_register', function( $wp_customize ) {

	// Move the background-color control.
	$wp_customize->get_control( 'background_color' )->section     = 'q_theme_grid';
	$wp_customize->get_control( 'background_color' )->priority    = 90;
	$wp_customize->get_control( 'background_color' )->description = esc_html__( 'Background is visible under transparent grid-parts, or if the grid is not set to 100% width.', 'q-theme' );

	// Move the background-image control.
	$wp_customize->get_control( 'background_image' )->section       = 'q_theme_grid';
	$wp_customize->get_control( 'background_image' )->priority      = 90;
	$wp_customize->get_control( 'background_image' )->description   = esc_html__( 'Background is visible under transparent grid-parts, or if the grid is not set to 100% width.', 'q-theme' );
	$wp_customize->get_control( 'background_preset' )->section      = 'q_theme_grid';
	$wp_customize->get_control( 'background_preset' )->priority     = 90;
	$wp_customize->get_control( 'background_position' )->section    = 'q_theme_grid';
	$wp_customize->get_control( 'background_position' )->priority   = 90;
	$wp_customize->get_control( 'background_size' )->section        = 'q_theme_grid';
	$wp_customize->get_control( 'background_size' )->priority       = 90;
	$wp_customize->get_control( 'background_repeat' )->section      = 'q_theme_grid';
	$wp_customize->get_control( 'background_repeat' )->priority     = 90;
	$wp_customize->get_control( 'background_attachment' )->section  = 'q_theme_grid';
	$wp_customize->get_control( 'background_attachment' )->priority = 90;

	// Move the header-image control.
	$wp_customize->get_control( 'header_image' )->section  = 'q_theme_grid_part_details_header';
	$wp_customize->get_control( 'header_image' )->priority = 80;
} );
