<?php
/**
 * Merlin WP configuration file.
 *
 * @package   Merlin WP
 * @version   @@pkg.version
 * @link      https://merlinwp.com/
 * @author    Rich Tabor, from ThemeBeans.com & the team at ProteusThemes.com
 * @copyright Copyright (c) 2018, Merlin WP of Inventionn LLC
 * @license   Licensed GPLv3 for Open Source Use
 */

if ( ! class_exists( 'Merlin' ) ) {
	return;
}

/**
 * Set directory locations, text strings, and settings.
 */
$wizard = new Merlin(
	[
		'directory'            => 'inc/merlin',
		'merlin_url'           => 'merlin',
		'parent_slug'          => 'themes.php',
		'capability'           => 'manage_options',
		'child_action_btn_url' => 'https://codex.wordpress.org/child_themes',
		'dev_mode'             => true,
		'license_step'         => true,
		'license_required'     => false,
		'license_help_url'     => '',
		'edd_remote_api_url'   => 'https://wplemon.com',
		'edd_item_name'        => 'q-theme',
		'edd_theme_slug'       => 'q-theme',
		'ready_big_button_url' => admin_url( 'customize.php' ),
	],
	[
		'admin-menu'               => esc_html__( 'Theme Setup', 'q-theme' ),

		/* translators: 1: Title Tag 2: Theme Name 3: Closing Title Tag */
		'title%s%s%s%s'            => esc_html__( '%1$s%2$s Themes &lsaquo; Theme Setup: %3$s%4$s', 'q-theme' ),
		'return-to-dashboard'      => esc_html__( 'Return to the dashboard', 'q-theme' ),
		'ignore'                   => esc_html__( 'Disable this wizard', 'q-theme' ),

		'btn-skip'                 => esc_html__( 'Skip', 'q-theme' ),
		'btn-next'                 => esc_html__( 'Next', 'q-theme' ),
		'btn-start'                => esc_html__( 'Start', 'q-theme' ),
		'btn-no'                   => esc_html__( 'Cancel', 'q-theme' ),
		'btn-plugins-install'      => esc_html__( 'Install', 'q-theme' ),
		'btn-child-install'        => esc_html__( 'Install', 'q-theme' ),
		'btn-content-install'      => esc_html__( 'Install', 'q-theme' ),
		'btn-import'               => esc_html__( 'Import', 'q-theme' ),
		'btn-license-activate'     => esc_html__( 'Activate', 'q-theme' ),
		'btn-license-skip'         => esc_html__( 'Later', 'q-theme' ),

		/* translators: Theme Name */
		'license-header%s'         => esc_html__( 'Activate %s', 'q-theme' ),
		/* translators: Theme Name */
		'license-header-success%s' => esc_html__( '%s is Activated', 'q-theme' ),
		/* translators: Theme Name */
		'license%s'                => esc_html__( 'Enter your license key to enable remote updates and theme support.', 'q-theme' ),
		'license-label'            => esc_html__( 'License key', 'q-theme' ),
		'license-success%s'        => esc_html__( 'The theme is already registered, so you can go to the next step!', 'q-theme' ),
		'license-json-success%s'   => esc_html__( 'Your theme is activated! Remote updates and theme support are enabled.', 'q-theme' ),
		'license-tooltip'          => esc_html__( 'Need help?', 'q-theme' ),

		/* translators: Theme Name */
		'welcome-header%s'         => esc_html__( 'Welcome to %s', 'q-theme' ),
		'welcome-header-success%s' => esc_html__( 'Hi. Welcome back', 'q-theme' ),
		'welcome%s'                => esc_html__( 'This wizard will set up your theme, install plugins, and import content. It is optional & should take only a few minutes.', 'q-theme' ),
		'welcome-success%s'        => esc_html__( 'You may have already run this theme setup wizard. If you would like to proceed anyway, click on the "Start" button below.', 'q-theme' ),

		'child-header'             => esc_html__( 'Install Child Theme', 'q-theme' ),
		'child-header-success'     => esc_html__( 'You\'re good to go!', 'q-theme' ),
		'child'                    => esc_html__( 'Let\'s build & activate a child theme so you may easily make theme changes.', 'q-theme' ),
		'child-success%s'          => esc_html__( 'Your child theme has already been installed and is now activated, if it wasn\'t already.', 'q-theme' ),
		'child-action-link'        => esc_html__( 'Learn about child themes', 'q-theme' ),
		'child-json-success%s'     => esc_html__( 'Awesome. Your child theme has already been installed and is now activated.', 'q-theme' ),
		'child-json-already%s'     => esc_html__( 'Awesome. Your child theme has been created and is now activated.', 'q-theme' ),

		'plugins-header'           => esc_html__( 'Install Plugins', 'q-theme' ),
		'plugins-header-success'   => esc_html__( 'You\'re up to speed!', 'q-theme' ),
		'plugins'                  => esc_html__( 'Let\'s install some essential WordPress plugins to get your site up to speed.', 'q-theme' ),
		'plugins-success%s'        => esc_html__( 'The required WordPress plugins are all installed and up to date. Press "Next" to continue the setup wizard.', 'q-theme' ),
		'plugins-action-link'      => esc_html__( 'Advanced', 'q-theme' ),

		'import-header'            => esc_html__( 'Import Content', 'q-theme' ),
		'import'                   => esc_html__( 'Let\'s import content to your website, to help you get familiar with the theme.', 'q-theme' ),
		'import-action-link'       => esc_html__( 'Advanced', 'q-theme' ),

		'ready-header'             => esc_html__( 'All done. Have fun!', 'q-theme' ),

		/* translators: Theme Author */
		'ready%s'                  => esc_html__( 'Your theme has been all set up. Enjoy your new theme by %s.', 'q-theme' ),
		'ready-action-link'        => esc_html__( 'Extras', 'q-theme' ),
		'ready-big-button'         => esc_html__( 'Start Customizing', 'q-theme' ),
		'ready-link-1'             => sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://wordpress.org/support/', esc_html__( 'Explore WordPress', 'q-theme' ) ),
		'ready-link-2'             => sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://wplemon.com/support/', esc_html__( 'Get Theme Support', 'q-theme' ) ),
		// 'ready-link-3'             => sprintf( '<a href="%1$s">%2$s</a>', admin_url( 'customize.php' ), esc_html__( 'Start Customizing', 'q-theme' ) ),
	]
);
