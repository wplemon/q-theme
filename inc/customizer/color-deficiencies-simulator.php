<?php
/**
 * Color Deficiencies Simulator.
 *
 * @package Q Theme
 * @since   1.0
 */

namespace Q_Theme\Customizer;

/**
 * A base for controls.
 */
class Color_Deficiencies_Simulator {

	/**
	 * Constructor.
	 *
	 * @since 1.0
	 * @access public
	 */
	public function __construct() {
		add_action( 'customize_controls_print_footer_scripts', [ $this, 'the_html' ], 5 );
		add_action( 'customize_controls_enqueue_scripts', [ $this, 'enqueue' ] );
	}

	/**
	 * Enqueue related scripts & styles.
	 *
	 * @access public
	 */
	public function enqueue() {
		wp_enqueue_script( 'q-color-deficiencies-simulator', get_template_directory_uri() . '/assets/js/customizer/color-deficiencies-simulator.js', [ 'jquery', 'customize-base', 'q-set-setting-value' ], Q_THEME_VERSION, false );
		wp_enqueue_style( 'q-color-deficiencies-simulator', get_template_directory_uri() . '/assets/css/customizer/color-deficiencies-simulator.css', [], Q_THEME_VERSION );
	}

	/**
	 * Adds the toolbar.
	 *
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function the_html() {
		?>
		<div id="q-a11y-colorblindness-sim" style="display:none;">
			<a id="q-color-deficiencies-simulator-trigger" href="#" title="<?php esc_html_e( 'Color Deficiencies Simulator', 'q-theme' ); ?>"><span class="dashicons dashicons-universal-access-alt"></span></a>
			<div id="q-color-deficiencies-sim-wrapper" aria-expanded="false">
				<h3><?php esc_html_e( 'Color Vision Deficiencies Simulator', 'q-theme' ); ?></h3>
				<span class="description">
					<p><strong><?php esc_html_e( 'Important Note: This feature currently only works in Firefox due to a browser bug in Safari and Chrome', 'q-theme' ); ?></strong></p>
					<?php esc_html_e( 'Simulates how people with a different perception of colors will see your website. Please note that this option does not get saved, it is merely a simulation.', 'q-theme' ); ?>
				</span>

				<div class="options">
					<label><input type="radio" name=q-accecss-selector" value="" checked=""><?php esc_html_e( 'No simulation', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="protanopia"><?php esc_html_e( 'Protanopia', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="protanomaly"><?php esc_html_e( 'Protanomaly', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="deuteranopia"><?php esc_html_e( 'Deuteranopia', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="deuteranomaly"><?php esc_html_e( 'Deuteranomaly', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="tritanopia"><?php esc_html_e( 'Tritanopia', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="tritanomaly"><?php esc_html_e( 'Tritanomaly', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="achromatopsia"><?php esc_html_e( 'Achromatopsia', 'q-theme' ); ?></label>
					<label><input type="radio" name=q-accecss-selector" value="achromatomaly"><?php esc_html_e( 'Achromatomaly', 'q-theme' ); ?></label>
				</div>
			</div>
		</div>
		<?php
	}
}
