<?php
/**
 * Q The-Grid control.
 *
 * @package Q Theme
 * @since 1.0
 */

namespace Q_Theme\Customizer\Control;

use Q_Theme\Grid_Parts;

/**
 * The grid control.
 *
 * @since 1.0
 */
class Grid extends \Kirki_Control_Base {

	/**
	 * The control type.
	 *
	 * @access public
	 * @var string
	 */
	public $type = 'q_theme_grid';

	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @access public
	 */
	public function enqueue() {

		// Enqueue ColorPicker.
		wp_enqueue_script( 'wp-color-picker-alpha', get_template_directory_uri() . '/assets/js/vendor/wp-color-picker-alpha.js', [ 'wp-color-picker' ], '2.0', true );

		// Enqueue the script and style.
		wp_enqueue_script( 'q-grid-control', get_template_directory_uri() . '/assets/js/customizer/q-grid-control.js', [ 'jquery', 'customize-base', 'wp-color-picker-alpha' ], Q_THEME_VERSION, false );

		wp_localize_script( 'q-grid-control', 'qGridControl', [
			'l10n'        => [
				'add'    => esc_attr__( 'Add', 'q-theme' ),
				'resize' => esc_attr__( 'Resize', 'q-theme' ),
				'edit'   => esc_attr__( 'Edit', 'q-theme' ),
				'delete' => esc_attr__( 'Delete', 'q-theme' ),
				'whatis' => [
					/* translators: The Column number. */
					'columnWidth' => esc_attr__( 'Column %d Width', 'q-theme' ),
					/* translators: The Row number. */
					'rowHeight'   => esc_attr__( 'Row %d Height', 'q-theme' ),
				],
			],
			'nestedParts' => Grid_Parts::get_instance()->get_grids(),
		] );
		wp_enqueue_style( 'q-grid-control', get_template_directory_uri() . '/assets/css/customizer/q-grid-control.css', [ 'wp-color-picker' ], Q_THEME_VERSION );
	}

	/**
	 * Render the control's content.
	 *
	 * @access protected
	 * @see WP_Customize_Control::render_content()
	 * @since 1.0
	 */
	protected function render_content() {
		$grid_parts = $this->choices['parts'];
		$value      = $this->value();
		// Sort parts by priority.
		usort( $grid_parts, function( $a, $b ) {
			if ( isset( $a['priority'] ) && isset( $b['priority'] ) ) {
				return ( $a['priority'] > $b['priority'] ) ? 1 : -1;
			}
			return ( isset( $a['priority'] ) ) ? 1 : -1;
		} );
		?>
		<!-- Label. -->
		<span class="customize-control-title">
			<?php echo $this->label; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		</span>

		<!-- Description. -->
		<span class="description customize-control-description">
			<?php echo $this->description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		</span>

		<!-- Grid Builder. -->
		<div class="q-grid-builder">

			<!-- Exit edit mode button. -->
			<?php foreach ( $grid_parts as $part ) : ?>
				<div class="template-part-advanced-options hidden" data-template-part-id="<?php echo esc_attr( $part['id'] ); ?>">
					<button class="button button-primary edit-part-options-done" data-template-part-id="<?php echo esc_attr( $part['id'] ); ?>">
						<?php esc_html_e( 'Exit edit mode', 'q-theme' ); ?>
					</button>
				</div>
			<?php endforeach; ?>

			<!-- Grid builder. -->
			<div class="grid-tab">
				<div class="q-grid-builder-grids-wrapper">
					<!-- Add action buttons. -->
					<div class="map-builder-actions">
						<button class="button button-secondary button add-column"><?php esc_html_e( '+ Column', 'q-theme' ); ?></button>
						<button class="button button-secondary button remove-column"><?php esc_html_e( '- Column', 'q-theme' ); ?></button>
						<button class="button button-secondary button add-row"><?php esc_html_e( '+ Row', 'q-theme' ); ?></button>
						<button class="button button-secondary button remove-row"><?php esc_html_e( '- Row', 'q-theme' ); ?></button>
					</div>

					<button class="grid-whatis"><span class="dashicons dashicons-editor-help"></span></button>
					<div class="q-grid-builder-columns"></div>
					<div class="q-grid-builder-rows"></div>

					<!-- Add a grid for each template part. -->
					<div class="q-grid-builder-grids">
						<div class="q-grid-selected"></div>
						<div id="q-grid-selectable-<?php echo esc_attr( $this->id ); ?>" class="q-grid-selectable"></div>
						<div class="q-grid-part-selector" style="display:none;">
							<h2><?php esc_html_e( 'Select Grid Part for this area.', 'q-theme' ); ?></h2>
							<select class="grid-part-select">
								<option value="" selected="selected"><?php esc_html_e( 'Select a grid-part to add.', 'q-theme' ); ?></option>
								<?php foreach ( $grid_parts as $part ) : ?>
									<?php if ( ! isset( $part['hidden'] ) || ! $part['hidden'] ) : ?>
										<option value="<?php echo esc_attr( $part['id'] ); ?>"><?php echo esc_html( $part['label'] ); ?></option>
									<?php endif; ?>
								<?php endforeach; ?>
							</select>
							<button class="q-grid-part-selector-cancel button"><?php esc_html_e( 'Cancel', 'q-theme' ); ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<input class="q-grid-hidden-value" type="hidden" value="<?php echo esc_attr( wp_json_encode( $value ) ); ?>" <?php $this->link(); ?>>
		<?php
	}
}
