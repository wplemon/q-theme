<?php
/**
 * Q The-Grid control.
 *
 * @package Q Theme
 * @since 1.0
 */

namespace Q_Theme\Customizer\Control;

/**
 * The grid control.
 *
 * @since 1.0
 */
class Color_A11y extends \WP_Customize_Control {

	/**
	 * The control type.
	 *
	 * @access public
	 * @var string
	 */
	public $type = 'q-color-a11y-checker';

	/**
	 * Whitelist the choices arg.
	 *
	 * @access public
	 * @since 1.0
	 * @var array
	 */
	public $choices = [];

	/**
	 * Whitelist the required arg.
	 *
	 * @access public
	 * @since 1.0
	 * @var array
	 */
	public $required = [];

	/**
	 * Refresh the parameters passed to the JavaScript via JSON.
	 *
	 * @access public
	 * @since 1.0
	 * @see WP_Customize_Control::to_json()
	 */
	public function to_json() {
		parent::to_json();
		$this->json['choices']  = $this->choices;
		$this->json['required'] = $this->required;
	}

	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @access public
	 */
	public function enqueue() {

		// Enqueue the script and style.
		wp_enqueue_script( 'q-color-a11y', get_template_directory_uri() . '/assets/js/customizer/q-color-a11y.js', [ 'jquery', 'customize-base', 'jquery-color', 'kirki-script' ], Q_THEME_VERSION, false );
		wp_enqueue_style( 'q-color-a11y', get_template_directory_uri() . '/assets/css/customizer/q-color-a11y-control.css', [], Q_THEME_VERSION );
	}

	/**
	 * Render the control's content.
	 *
	 * @access protected
	 * @see WP_Customize_Control::render_content()
	 * @since 1.0
	 */
	protected function render_content() {
	}
}
