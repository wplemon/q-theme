<?php
/**
 * Q Help control.
 *
 * @package Q Theme
 * @since 1.0
 */

namespace Q_Theme\Customizer\Control;

/**
 * The help control.
 *
 * @since 1.0
 */
class Help extends \Kirki_Control_Base {

	/**
	 * The control type.
	 *
	 * @access public
	 * @var string
	 */
	public $type = 'q_section_help';

	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @access public
	 */
	public function enqueue() {
		wp_enqueue_script( 'q-section-help-control', get_template_directory_uri() . '/assets/js/customizer/q-help-section-control.js', [ 'jquery', 'customize-base', 'wp-api' ], Q_THEME_VERSION, false );
		wp_enqueue_style( 'q-section-help-control', get_template_directory_uri() . '/assets/css/customizer/q-section-help-control.css', [], Q_THEME_VERSION );
	}

	/**
	 * Render the control's content.
	 *
	 * @access protected
	 * @see WP_Customize_Control::render_content()
	 * @since 1.0
	 */
	protected function render_content() {
		?>
		<!-- Label. -->
		<span class="customize-control-title">
			<?php echo $this->label; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		</span>

		<!-- Description. -->
		<span class="description customize-control-description">
			<?php echo $this->description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		</span>

		<a href="#" class="q-help-section-link">
			<span class="expand"><?php echo esc_html( $this->choices['link_label_expand'] ); ?></span>
			<span class="collapse"><?php echo esc_html( $this->choices['link_label_collapse'] ); ?></span>
		</a>
		<div class="q-section-help-wrapper<?php echo isset( $this->choices['content'] ) ? ' content-ready' : ''; ?>">
			<div class="q-section-help-content">
				<?php
				if ( isset( $this->choices['content'] ) ) {
					// This value is hardcoded, no need to escape it.
					echo $this->choices['content']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				}
				?>
			</div>
		</div>
		<?php
	}
}
