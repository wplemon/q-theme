/* global wcagColors */
wp.customize.controlConstructor['kirki-wcag-tc'] = wp.customize.Control.extend({

	/**
	 * The current mode of operation.
	 *
	 * @since 1.0
	 */
	a11ySelectorMode: false,

	/**
	 * An array of accessible colors.
	 *
	 * @since 1.0
	 */
	colors: [],

	/**
	 * The maximum hue difference.
	 *
	 * @since 1.0
	 */
	maxHueDiff: 60,

	/**
	 * The steps for hue. Integer.
	 * Smaller values make for a more detailed search.
	 *
	 * @since 1.0
	 */
	stepHue: 15,

	/**
	 * The maximum Saturation for accessible colors.
	 *
	 * @since 1.0
	 */
	maxSaturation: 0.5,

	/**
	 * The steps for saturation. Float value, 0 to 1.
	 * Smaller values make for a more detailed search.
	 *
	 * @since 1.0
	 */
	stepSaturation: 0.1,

	/**
	 * The steps for lightness. Float value, 0 to 1.
	 * Smaller values make for a more detailed search.
	 *
	 * @since 1.0
	 */
	stepLightness: 0.05,

	/**
	 * The contrast threshold.
	 *
	 * @since 1.0
	 */
	contrastThreshold: 4.5,

	/**
	 * Triggered when the control is ready.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	ready: function() {
		var control = this;

		// Set the background color.
		this.setBackgroundColorDetails();

		// Get new array of accessible colors.
		// this.setControlColors();

		// Set control params if defined in the choices argument.
		if ( 'undefined' !== typeof control.params.choices.maxHueDiff ) {
			this.maxHueDiff = control.params.choices.maxHueDiff;
		}
		if ( 'undefined' !== typeof control.params.choices.stepHue ) {
			this.stepHue = control.params.choices.stepHue;
		}
		if ( 'undefined' !== typeof control.params.choices.maxSaturation ) {
			this.maxSaturation = control.params.choices.maxSaturation;
		}
		if ( 'undefined' !== typeof control.params.choices.stepSaturation ) {
			this.stepSaturation = control.params.choices.stepSaturation;
		}
		if ( 'undefined' !== typeof control.params.choices.stepLightness ) {
			this.stepLightness = control.params.choices.stepLightness;
		}
		if ( 'undefined' !== typeof control.params.choices.contrastThreshold ) {
			this.contrastThreshold = control.params.choices.contrastThreshold;
		}

		this.updateColorsDebounced  = _.debounce( _.bind( this.updateColors, this ), 100 );
		this.watchSettingsDebounced = _.debounce( _.bind( this.watchSettings, this ), 100 );

		setTimeout( function() {

			// Init tabs.
			control.initTabs();
		}, 300 );

		// Init the colorpicker.
		this.initColorpicker();

		// Generate the control.
		this.updateColorsDebounced();

		// Watch for changes.
		this.watchSettingsDebounced();
	},

	/**
	 * Init the tabs for recommended/custom colors.
	 *
	 * @since 1.0
	 * @param {bool} forceTab - Whether we want to force the tab switch or not.
	 * @returns {void}
	 */
	initTabs: function( forceTab ) {
		var control = this;

		// Remove previous even listeners.
		jQuery( control.container.find( '.tabs button' ) ).off();

		// Handle clicking buttons.
		jQuery( control.container.find( '.tabs button' ) ).on( 'click', function( event ) {
			control.a11ySelectorMode = jQuery( event.target ).attr( 'data-mode' );
			control.initTabs( true );
			event.preventDefault();
			if ( 'auto' === control.a11ySelectorMode ) {
				jQuery( '.kirki-color-control[data-id="' + control.id + '"]' ).attr( 'value', control.getAutoColor() ).trigger( 'change' );
			}
		});

		if ( ! forceTab && ! control.a11ySelectorMode ) {

			if ( control.setting._value === control.getAutoColor() ) {
				control.a11ySelectorMode = 'auto';
			} else {
				control.a11ySelectorMode = 'recommended';

			}
			control.initTabs();
			return;
		}

		jQuery( control.container.find( '.tabs button' ) ).removeClass( 'active' );
		jQuery( control.container.find( '.tabs button[data-mode="' + control.a11ySelectorMode + '"]' ) ).addClass( 'active' );
		jQuery( control.container.find( '.mode-selectors' ) ).attr( 'data-value', control.a11ySelectorMode );
		jQuery( control.container.find( '.kirki-a11y-text-colorpicker-wrapper' ) ).hide();
		jQuery( control.container.find( '.kirki-a11y-text-colorpicker-wrapper[data-id="' + control.a11ySelectorMode + '"]' ) ).show();

		if ( 'auto' === control.a11ySelectorMode ) {
			jQuery( control.container.find( '.selected-color-previewer-auto' ) )
				.html( control.getAutoColor() )
				.css( 'background-color', this.getColor() )
				.css( 'color', control.getAutoColor() );
		}
	},

	/**
	 * Check if the selected color is one of the recommended colors or not.
	 *
	 * @since 1.0
	 * @returns {bool}
	 */
	isSelectedColorRecommended: function() {
		return ( this.colors && 'undefined' !== typeof this.colors[ this.setting._value ] );
	},

	/**
	 * Init the colorpicker.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	initColorpicker: function() {
		var control = this,
			picker = jQuery( control.container.find( '.kirki-color-control' ) ),
			clear;

		// Tweaks to make the "clear" buttons work.
		setTimeout( function() {
			clear = jQuery( control.container.find( '.kirki-input-container[data-id="' + control.id + '"] .wp-picker-clear' ) );
			if ( clear.length ) {
				clear.click( function() {
					wp.customize.control( control.id ).setting.set( '' );
				});
			}
		}, 200 );

		// Saves our settings to the WP API
		picker.wpColorPicker({
			change: function() {

				// Small hack: the picker needs a small delay
				setTimeout( function() {
					wp.customize.control( control.id ).setting.set( picker.val() );
				}, 20 );
			}
		});
	},

	/**
	 * The main init method.
	 *
	 * @since 1.0
	 * @param {string} newColor - The new color.
	 * @returns {void}
	 */
	updateColors: function( newColor ) {
		var control = this;

		if ( ! this.backgroundColorChanged( newColor ) ) {
			return;
		}

		// Update the colors in the object.
		this.setBackgroundColorDetails();

		// Get new array of accessible colors.
		this.setControlColors();

		// Update the HTML for the control.
		this.updateControlHTML();

		// Handle auto mode.
		if ( 'auto' === control.a11ySelectorMode ) {

			// Change the value in the colorpicker.
			// Automatically sets the value in the customizer object
			jQuery( '.kirki-color-control[data-id="' + control.id + '"]' ).attr( 'value', control.getAutoColor() ).trigger( 'change' );

			// Display the value.
			jQuery( control.container.find( '.selected-color-previewer-auto' ) )
				.html( control.getAutoColor() )
				.css( 'background-color', this.getColor() )
				.css( 'color', control.getAutoColor() );
		}
	},

	/**
	 * Updates the HTML in the control. adding the colors we need.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	updateControlHTML: function() {
		var control         = this,
			colorsContainer = jQuery( control.container.find( '.kirki-a11y-text-colorpicker-wrapper[data-id="recommended"] .wrapper' ) ),
			html            = '',
			borderColor;

		// Reset the HTML.
		colorsContainer.html( '' );

		// Add colors as radio-buttons.
		_.each( control.colors, function( color ) {
			borderColor = ( 0.5 < color.lum ) ? 'rgba(0,0,0,.15)' : 'rgba(255,255,255,.15)';
			html += '<label>';
			html += '<input type="radio" value="' + color.hex + '" name="_customize-radio-' + control.id + '"';
			html += ( control.setting._value === color.hex ) ? ' checked' : '';
			html += ' ' + control.params.link;
			html += '/>';
			html += '<span class="a11y-text-selector-label" style="background-color:' + color.hex + ';border-color:' + borderColor + ';"></span>';
			html += '<span class="screen-reader-text">' + color.hex + '</span>';
			html += '</label>';
		});

		colorsContainer.append( html );

		// Watch for value changes and save if necessary.
		control.saveColorValueWatcher();
	},

	/**
	 * Watch defined controls and re-trigger results calculations when there's a change.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	watchSettings: function() {
		var control = this;

		wp.customize( control.params.choices.setting, function( setting ) {
			setting.bind( function( to ) {
				control.updateColors( to );
			});
		});

		if ( -1 < control.params.choices.setting.indexOf( '[' ) ) {
			wp.customize( control.params.choices.setting.split( '[' )[0], function( setting ) {
				setting.bind( function( to ) {
					control.updateColors( to );
				});
			});
		}
	},

	/**
	 * Sets items in the this.colors array.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	setControlColors: function() {
		this.colors = wcagColors.getAll({
			color: this.backgroundColor.hex,
			maxHueDiff: this.backgroundColor.h ? this.maxHueDiff : 360,
			stepHue: this.stepHue,
			minSaturation: 0,
			maxSaturation: this.maxSaturation,
			stepSaturation: this.stepSaturation,
			stepLightness: this.stepLightness
		}).pluck({
			color: this.backgroundColor.hex,
			minContrast: this.contrastThreshold
		}).sortBy( 'contrast' ).colors;
	},

	/**
	 * Automatically gets the color with the most contrast.
	 *
	 * @since 1.0
	 * @returns {string}
	 */
	getAutoColor: function() {
		return ( this.colors[0] && this.colors[0].hex ) ? this.colors[0].hex : '#000000';
	},

	/**
	 * Set the object's colors property.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	setBackgroundColorDetails: function() {
		this.backgroundColor = wcagColors.getColorProperties( this.getColor() );
	},

	/**
	 * Gets a color defined in the control's choices.
	 *
	 * @since 1.0
	 * @returns {string}
	 */
	getColor: function() {
		return wp.customize( this.params.choices.setting ).get();
	},

	/**
	 * Check if the background color has changed.
	 *
	 * @since 1.0
	 * @param {string} newColor - The new color.
	 * @returns {bool}
	 */
	backgroundColorChanged: function( newColor ) {
		var control      = this,
			colorChanged = false;

		if ( newColor !== control.backgroundColor.hex ) {
			colorChanged = true;
			if ( newColor ) {
				control.backgroundColor = wcagColors.getColorProperties( newColor );
			}
		}
		return colorChanged;
	},

	/**
	 * Sets the value for this control.
	 *
	 * @returns {void}
	 */
	saveColorValueWatcher: function() {
		var control = this;

		jQuery( control.container.find( '.kirki-a11y-text-colorpicker-wrapper[data-id="recommended"] input' ) ).on( 'change keyup paste click', function() {

			// Change the value in the colorpicker.
			// Automatically sets the value in the customizer object
			jQuery( '.kirki-color-control[data-id="' + control.id + '"]' ).attr( 'value', jQuery( this ).val() ).trigger( 'change' );

			// Focus on this element. Enhases keyboard-navigation experience.
			jQuery( this ).focus();
		});

		// Update the indicator value.
		this.setting.bind( 'change', function( to ) {
			jQuery( control.container.find( '.selected-color' ) ).html( to );
		});
	}
});
