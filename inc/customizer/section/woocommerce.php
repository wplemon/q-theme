<?php
/**
 * Customizer Blog Options.
 *
 * @package Q Theme
 */

if ( ! class_exists( 'Kirki' ) ) {
	return;
}

q_theme_add_customizer_field( [
	'type'        => 'dimension',
	'settings'    => 'q_theme_woocommerce_product_catalog_min_width',
	'label'       => esc_attr__( 'Products min-width', 'q-theme' ),
	'description' => '',
	'section'     => 'woocommerce_product_catalog',
	'transport'   => 'auto',
	'default'     => '250px',
	'css_vars'    => '--q-woo-catalog-product-min-width',
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_woocommerce_product_catalog_per_page',
	'label'       => esc_attr__( 'Products per-page', 'q-theme' ),
	'description' => '',
	'section'     => 'woocommerce_product_catalog',
	'default'     => 12,
	'choices'     => [
		'min'  => 1,
		'max'  => 100,
		'step' => 1,
	],
] );
