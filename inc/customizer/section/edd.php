<?php
/**
 * Customizer Easy Digital Downloads Options.
 *
 * @package Q Theme
 */

if ( ! class_exists( 'Kirki' ) ) {
	return;
}

if ( ! class_exists( 'Easy_Digital_Downloads' ) ) {
	return;
}

q_theme_add_customizer_panel( 'q_theme_edd', [
	'title'    => esc_attr__( 'Easy Digital Downloads', 'q-theme' ),
	'priority' => 30,
	// 'panel'    => 'q_theme_options',
] );

q_theme_add_customizer_section( 'q_theme_edd_grid', [
	'title'       => esc_attr__( 'Product Grid', 'q-theme' ),
	'panel'       => 'q_theme_edd',
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/edd/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_edd_grid_min_col_width',
	'label'       => esc_attr__( 'Minimum Column Width', 'q-theme' ),
	'description' => esc_html__( 'Define the minimum width that each item in a grid can have. The columns and rows will be auto-calculated using this value.', 'q-theme' ),
	'section'     => 'q_theme_edd_grid',
	'default'     => 320,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-edd-grid-min-col-width', '$px' ],
	'choices'     => [
		'min'    => 200,
		'max'    => 600,
		'step'   => 1,
		'suffix' => 'px',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_edd_archive_grid_gap',
	'label'       => esc_attr__( 'Gap', 'q-theme' ),
	'description' => esc_html__( 'The gap between grid items. Use any valid CSS value.', 'q-theme' ),
	'section'     => 'q_theme_edd_grid',
	'default'     => 20,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-edd-grid-gap', '$px' ],
	'choices'     => [
		'min'    => 0,
		'max'    => 200,
		'step'   => 1,
		'suffix' => 'px',
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'slider',
	'settings'  => 'q_theme_edd_product_grid_inner_padding',
	'label'     => esc_attr__( 'Grid Items Inner Padding', 'q-theme' ),
	'section'   => 'q_theme_edd_grid',
	'default'   => 20,
	'transport' => 'postMessage',
	'css_vars'  => [ '--q-edd-grid-inner-padding', '$px' ],
	'choices'   => [
		'min'    => 0,
		'max'    => 60,
		'step'   => 1,
		'suffix' => 'px',
	],
] );
