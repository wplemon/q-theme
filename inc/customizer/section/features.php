<?php
/**
 * Customizer Blog Options.
 *
 * @package Q Theme
 */

if ( ! class_exists( 'Kirki' ) ) {
	return;
}

q_theme_add_customizer_section( 'q_theme_features', [
	'title'       => esc_attr__( 'Theme Features', 'q-theme' ),
	'priority'    => 28,
	'description' => '<a href="https://wplemon.com/documentation/q-theme/theme-features-customizer-section/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	// 'panel'    => 'q_theme_options',
] );

q_theme_add_customizer_section( 'q_theme_features_widget_areas', [
	'title'       => esc_attr__( 'Widget Areas', 'q-theme' ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/theme-features-customizer-section/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'section'     => 'q_theme_features',
] );

$max_widget_areas = apply_filters( 'q_theme_max_widget_areas', 15 );

q_theme_add_customizer_field( [
	'type'        => 'sortable',
	'settings'    => 'q_theme_post_parts',
	'label'       => esc_html__( 'Posts Components', 'q-theme' ),
	'description' => esc_html__( 'Select the active components for posts/pages and their order. Changes here will affect both single posts/pages and archives.', 'q-theme' ),
	'section'     => 'q_theme_features',
	'default'     => [ 'post-title', 'post-date-author', 'post-thumbnail', 'post-content', 'post-category', 'post-tags', 'post-comments-link' ],
	'transport'   => 'refresh',
	'choices'     => [
		'post-title'         => esc_html__( 'Title', 'q-theme' ),
		'post-date-author'   => esc_html__( 'Post-Date & Author Name', 'q-theme' ),
		'post-thumbnail'     => esc_html__( 'Featured Image', 'q-theme' ),
		'post-content'       => esc_html__( 'Content', 'q-theme' ),
		'post-category'      => esc_html__( 'Post Category', 'q-theme' ),
		'post-tags'          => esc_html__( 'Post Tags', 'q-theme' ),
		'post-comments-link' => esc_html__( 'Comments Link (on archives only)', 'q-theme' ),
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'radio',
	'settings'        => 'q_theme_featured_image_mode_archive',
	'label'           => esc_attr__( 'Featured Images Mode in Archives', 'q-theme' ),
	'description'     => esc_html__( 'Select how featured images will be displayed in post-archives.', 'q-theme' ),
	'section'         => 'q_theme_features',
	'default'         => 'alignwide',
	'transport'       => 'refresh',
	'choices'         => [
		'hidden'    => esc_attr__( 'Hidden', 'q-theme' ),
		'q-contain' => esc_attr__( 'Normal', 'q-theme' ),
		'alignwide' => esc_attr__( 'Wide', 'q-theme' ),
	],
	'active_callback' => function() {
		return ( is_archive() || is_home() );
	},
] );

q_theme_add_customizer_field( [
	'type'            => 'radio',
	'settings'        => 'q_theme_featured_image_mode_singular',
	'label'           => esc_attr__( 'Featured Images Mode in Single Posts', 'q-theme' ),
	'description'     => esc_html__( 'Select how featured images will be displayed in single post-types (Applies to all post-types).', 'q-theme' ),
	'section'         => 'q_theme_features',
	'default'         => 'alignwide',
	'transport'       => 'refresh',
	'choices'         => [
		'hidden'    => esc_attr__( 'Hidden', 'q-theme' ),
		'q-contain' => esc_attr__( 'Normal', 'q-theme' ),
		'alignwide' => esc_attr__( 'Wide', 'q-theme' ),
		'alignfull' => esc_attr__( 'Full Width', 'q-theme' ),
		'parallax'  => esc_attr__( 'Parallax', 'q-theme' ),
	],
	'active_callback' => function() {
		return is_singular();
	},
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_featured_image_parallax_singular_height',
	'label'           => esc_attr__( 'Parallax container maximum height', 'q-theme' ),
	'description'     => esc_html__( 'Select how featured images will be displayed in single post-types (Applies to all post-types).', 'q-theme' ),
	'section'         => 'q_theme_features',
	'default'         => '60vh',
	'transport'       => 'refresh',
	'css_vars'        => '--q-featured-image-parallax-height',
	'transport'       => 'postMessage',
	'active_callback' => function() {
		return ( is_singular() && 'parallax' === get_theme_mod( 'q_theme_featured_image_mode_singular', 'alignwide' ) );
	},
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_widget_areas_number',
	'label'       => esc_attr__( 'Number of custom widget areas', 'q-theme' ),
	'description' => __( 'Select how many custom widget areas you want to add.', 'q-theme' ),
	'section'     => 'q_theme_features_widget_areas',
	'default'     => 2,
	'transport'   => 'postMessage',
	'choices'     => [
		'min'  => 0,
		'max'  => 15,
		'step' => 1,
	],
] );

$range = range( 1, $max_widget_areas, 1 );
foreach ( $range as $i ) {
	q_theme_add_customizer_field( [
		'type'            => 'text',
		'settings'        => "q_theme_grid_widget_area_{$i}_name",
		/* translators: The widget-area number. */
		'label'           => sprintf( esc_html__( 'Widget Area %d Name', 'q-theme' ), absint( $i ) ),
		'description'     => esc_html__( 'Enter a custom name if you want to easier identify this widget-area', 'q-theme' ),
		'section'         => 'q_theme_features_widget_areas',
		/* translators: The widget-area number. */
		'default'         => sprintf( esc_html__( 'Widget Area %d', 'q-theme' ), absint( $i ) ),
		'transport'       => 'postMessage',
		'active_callback' => [
			[
				'setting'  => 'q_theme_grid_widget_areas_number',
				'operator' => '>=',
				'value'    => $i,
			],
		],
	] );
}

q_theme_add_customizer_field( [
	'type'        => 'switch',
	'settings'    => 'q_theme_show_next_prev',
	'label'       => esc_attr__( 'Show Next/Previous Post', 'q-theme' ),
	'description' => esc_html__( 'Shows next/previous post buttons on the bottom of single posts.', 'q-theme' ),
	'section'     => 'q_theme_features',
	'default'     => true,
	'transport'   => 'refresh',
] );

q_theme_add_customizer_field( [
	'type'        => 'switch',
	'settings'    => 'q_theme_headers_anchor_links',
	'label'       => esc_attr__( 'Enable anchor links in headers', 'q-theme' ),
	'description' => esc_html__( 'If a header block has an anchor defined, this will add a link icon at the end of the headers when they are hovered so that users can easier link to sections of your pages.', 'q-theme' ),
	'section'     => 'q_theme_features',
	'default'     => true,
	'transport'   => 'refresh',
] );

q_theme_add_customizer_field( [
	'type'        => 'radio',
	'settings'    => 'q_theme_enable_totop',
	'label'       => esc_attr__( 'Enable scroll-to-top button.', 'q-theme' ),
	'description' => esc_html__( 'When on a long page shows a "Scroll-to-top" button on the bottom-right corner of the screen. Select "Desktop" to only show the button on large screens, "Show" to show the button on both large and small screens, or "Hidden" to disable the button.', 'q-theme' ),
	'section'     => 'q_theme_features',
	'default'     => 'hidden',
	'transport'   => 'refresh',
	'choices'     => [
		'hidden' => esc_attr__( 'Hidden', 'q-theme' ),
		'large'  => esc_attr__( 'Desktop Only', 'q-theme' ),
		'all'    => esc_attr__( 'Show', 'q-theme' ),
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'radio',
	'settings'    => 'q_theme_archive_mode',
	'label'       => esc_attr__( 'Post display mode in archives', 'q-theme' ),
	'description' => '',
	'section'     => 'q_theme_features',
	'default'     => 'excerpt',
	'transport'   => 'refresh',
	'choices'     => [
		'excerpt' => esc_html__( 'Excerpt', 'q-theme' ),
		'full'    => esc_attr__( 'Full Post', 'q-theme' ),
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'textarea',
	'settings'    => 'q_theme_excerpt_more',
	'label'       => esc_attr__( 'Read More link', 'q-theme' ),
	'description' => esc_html__( 'Available placeholder: %s for the post-title.', 'q-theme' ), // phpcs:ignore WordPress.WP.I18n.MissingTranslatorsComment
	'section'     => 'q_theme_features',
	/* translators: %s: Name of current post. Only visible to screen readers */
	'default'     => __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'q-theme' ),
	'transport'   => 'refresh',
] );
