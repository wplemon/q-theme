<?php
/**
 * Customizer Grid Options.
 *
 * @package Q Theme
 */

use Q_Theme\Customizer\Control\Color_A11y;
use Q_Theme\Grid_Parts;

if ( ! class_exists( 'Kirki' ) ) {
	return;
}

$grid_parts = Grid_Parts::get_instance()->get_parts();

q_theme_add_customizer_section( 'q_theme_grid', [
	'title'       => esc_attr__( 'Grid', 'q-theme' ),
	'priority'    => 22,
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
] );

/**
 * Global toggle for mobile/desktop grid.
 */
q_theme_add_customizer_field( [
	'settings'          => 'q_theme_global_grid_toggle',
	'type'              => 'radio-buttonset',
	'label'             => esc_html__( 'Toggle between desktop & mobile grids.', 'q-theme' ),
	'section'           => 'q_theme_grid',
	'default'           => 'desktop',
	'priority'          => -10,
	'transport'         => 'postMessage',
	'choices'           => [
		'desktop' => esc_html__( 'Desktop', 'q-theme' ),
		'mobile'  => esc_html__( 'Mobile', 'q-theme' ),
	],
	'sanitize_callback' => function() {
		return 'desktop';
	},
] );

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_global_grid_preset',
	'type'              => 'radio-image',
	'label'             => esc_html__( 'Main grid Preset', 'q-theme' ),
	'description'       => esc_html__( 'Quickly changes the main grid to some common layouts', 'q-theme' ),
	'default'           => '',
	'transport'         => 'postMessage',
	'section'           => 'q_theme_grid',
	'sanitize_callback' => '__return_empty_string',
	'choices'           => [
		'1-col-wide'       => get_template_directory_uri() . '/assets/images/layouts/1-col-wide.png',
		'1-col-boxed'      => get_template_directory_uri() . '/assets/images/layouts/1-col-boxed.png',
		'2-col-left-wide'  => get_template_directory_uri() . '/assets/images/layouts/2-col-left-wide.png',
		'2-col-right-wide' => get_template_directory_uri() . '/assets/images/layouts/2-col-right-wide.png',
	],
	'preset'            => [
		'1-col-wide'       => [
			'settings' => [
				'q_theme_grid'                  => [
					'rows'         => 3,
					'columns'      => 1,
					'areas'        => [
						'header'  => [
							'cells' => [ [ 1, 1 ] ],
						],
						'content' => [
							'cells' => [ [ 2, 1 ] ],
						],
						'footer'  => [
							'cells' => [ [ 3, 1 ] ],
						],
					],
					'gridTemplate' => [
						'rows'    => [ 'auto', 'auto', 'auto' ],
						'columns' => [ 'auto' ],
					],
				],
				'q_theme_grid_mobile_mode'      => 'default',
				'q_mobile_breakpoint'           => '900px',
				'q_theme_grid_max_width'        => '',
				'q_theme_grid_header_max_width' => '',
				'q_theme_grid_footer_max_width' => '',
			],
		],
		'1-col-boxed'      => [
			'settings' => [
				'q_theme_grid'                  => [
					'rows'         => 3,
					'columns'      => 1,
					'areas'        => [
						'header'  => [
							'cells' => [ [ 1, 1 ] ],
						],
						'content' => [
							'cells' => [ [ 2, 1 ] ],
						],
						'footer'  => [
							'cells' => [ [ 3, 1 ] ],
						],
					],
					'gridTemplate' => [
						'rows'    => [ 'auto', 'auto', 'auto' ],
						'columns' => [ 'auto' ],
					],
				],
				'q_theme_grid_mobile_mode'      => 'default',
				'q_mobile_breakpoint'           => '900px',
				'q_theme_grid_max_width'        => '50em',
				'q_theme_grid_header_max_width' => '',
				'q_theme_grid_footer_max_width' => '',
			],
		],
		'2-col-left-wide'  => [
			'settings' => [
				'q_theme_grid'                  => [
					'rows'         => 3,
					'columns'      => 2,
					'areas'        => [
						'header'    => [
							'cells' => [ [ 1, 1 ], [ 1, 2 ] ],
						],
						'content'   => [
							'cells' => [ [ 2, 2 ] ],
						],
						'sidebar_1' => [
							'cells' => [ [ 2, 1 ] ],
						],
						'footer'    => [
							'cells' => [ [ 3, 1 ], [ 3, 2 ] ],
						],
					],
					'gridTemplate' => [
						'rows'    => [ 'auto', 'auto', 'auto' ],
						'columns' => [ '15em', '1fr' ],
					],
				],
				'q_theme_grid_mobile_mode'      => 'default',
				'q_mobile_breakpoint'           => '900px',
				'q_theme_grid_max_width'        => '',
				'q_theme_grid_header_max_width' => '',
				'q_theme_grid_footer_max_width' => '',
			],
		],
		'2-col-right-wide' => [
			'settings' => [
				'q_theme_grid'                  => [
					'rows'         => 3,
					'columns'      => 2,
					'areas'        => [
						'header'    => [
							'cells' => [ [ 1, 1 ], [ 1, 2 ] ],
						],
						'content'   => [
							'cells' => [ [ 2, 1 ] ],
						],
						'sidebar_1' => [
							'cells' => [ [ 2, 2 ] ],
						],
						'footer'    => [
							'cells' => [ [ 3, 1 ], [ 3, 2 ] ],
						],
					],
					'gridTemplate' => [
						'rows'    => [ 'auto', 'auto', 'auto' ],
						'columns' => [ '1fr', '15em' ],
					],
				],
				'q_theme_grid_max_width'        => '',
				'q_theme_grid_header_max_width' => '',
				'q_theme_grid_footer_max_width' => '',
			],
		],
	],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_grid',
	'section'           => 'q_theme_grid',
	'type'              => 'q_theme_grid',
	'grid-part'         => false,
	'label'             => esc_html__( 'Grid Settings', 'q-theme' ),
	'description'       => sprintf(
		/* translators: Link attributes. */
		__( 'Edit settings for the grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
	),
	'default'           => q_theme_get_grid_default_value(),
	'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
	'choices'           => [
		'parts'     => Grid_Parts::get_instance()->get_parts(),
		'duplicate' => 'q_theme_grid_mobile',
	],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_mobile_breakpoint',
	'label'           => esc_html__( 'Mobile Breakpoint', 'q-theme' ),
	'description'     => esc_html__( 'The breakpoint that separates mobile views from desktop views. Use a valid CSS unit.', 'q-theme' ),
	'tooltip'         => __( 'It is good practice to change the layout in smaller devices in order to accomodate for their viewport size. You can use this setting to change the threshold at which your layouts will change. If you want to change the loading order of grid-parts for your mobile users you can do so using the "Grid Parts Order" setting below.', 'q-theme' ),
	'section'         => 'q_theme_grid',
	'default'         => '800px',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_grid_mobile_mode',
	'section'         => 'q_theme_grid',
	'type'            => 'radio-buttonset',
	'label'           => esc_html__( 'Grid Small Viewports Mode', 'q-theme' ),
	'description'     => esc_html__( 'If you want to customize the way your grid will be shown in mobile and small devices, you can set this option to "Custom" and create a separate grid for mobile.', 'q-theme' ),
	'default'         => 'default',
	'choices'         => [
		'default' => esc_attr__( 'Default', 'q-theme' ),
		'custom'  => esc_attr__( 'Custom', 'q-theme' ),
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_copy_main_grid_to_mobile',
	'type'            => 'custom',
	'label'           => esc_html__( 'Copy main grid setings to mobile grid', 'q-theme' ),
	'section'         => 'q_theme_grid',
	'default'         => '<div style="margin-bottom:1em;"><button class="button-q-copy-grid-setting button button-primary button-large" data-from="q_theme_grid" data-to="q_theme_grid_mobile">' . esc_html__( 'Click here to copy settings from main grid', 'q-theme' ) . '</button></div>',
	'active_callback' => [
		[
			'setting'  => 'q_theme_grid_mobile_mode',
			'value'    => 'custom',
			'operator' => '===',
		],
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_grid_mobile',
	'section'           => 'q_theme_grid',
	'type'              => 'q_theme_grid',
	'grid-part'         => false,
	'label'             => esc_html__( 'Mobile Grid Settings', 'q-theme' ),
	'description'       => sprintf(
		/* translators: Link attributes. */
		__( 'Edit settings for the grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
	),
	'default'           => q_theme_get_grid_default_value(),
	'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
	'choices'           => [
		'parts'              => Grid_Parts::get_instance()->get_parts(),
		'duplicate'          => 'q_theme_grid',
		'disablePartButtons' => [ 'edit' ],
	],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_grid_mobile_mode',
			'value'    => 'custom',
			'operator' => '===',
		],
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_gap',
	'label'           => esc_attr__( 'Grid Container Gap', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'grid-gap-description' ),
	'tooltip'         => esc_html__( 'If you have a background-color defined for your site, then that color will be visible through these gaps which creates a unique appearance since each grid-part looks separate.', 'q-theme' ),
	'section'         => 'q_theme_grid',
	'default'         => '0',
	'transport'       => 'auto',
	'output'          => [
		[
			'element'  => '.q-site-wrapper',
			'property' => 'grid-gap',
		],
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_max_width',
	'label'           => esc_attr__( 'Grid Container max-width', 'q-theme' ),
	'description'     => esc_html__( 'The maximum width for this grid.', 'q-theme' ),
	'tooltip'         => esc_html__( 'By setting the max-width to something other than 100% you get a boxed layout.', 'q-theme' ),
	'section'         => 'q_theme_grid',
	'default'         => '',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-grid-max-width',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );
