<?php
/**
 * Customizer Typography Options.
 *
 * @package Q Theme
 */

use Q_Theme\Customizer\Control\Color_A11y;

if ( ! class_exists( 'Kirki' ) ) {
	return;
}

/**
 * Add the Theme-Options panel.
 */
q_theme_add_customizer_section( 'q_theme_typography', [
	'title'       => esc_attr__( 'Typography & Links', 'q-theme' ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/typography/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 20,
	// 'panel'    => 'q_theme_options',
] );

/**
 * Body typography.
 */
q_theme_add_customizer_field( [
	'type'        => 'typography',
	'settings'    => 'q_theme_body_typography',
	'label'       => esc_html__( 'Body Typography', 'q-theme' ),
	'description' => esc_html__( 'Edit the main typography settings for your site.', 'q-theme' ),
	'tooltip'     => esc_html__( 'The font-size selected here will affect all other elements on your site proportionally', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'priority'    => 10,
	'default'     => [
		'font-family' => 'Noto Serif',
		'variant'     => 'regular',
	],
	'transport'   => 'auto',
	'output'      => [
		[
			'element' => 'body',
		],
		[
			'element' => '.edit-post-visual-editor.editor-styles-wrapper',
			'context' => [ 'editor' ],
		],
	],
	'choices'     => [
		'fonts' => [
			'google' => [ 'popularity' ],
		],
	],
] );

/**
 * Headers typography.
 */
q_theme_add_customizer_field( [
	'type'        => 'typography',
	'settings'    => 'q_theme_headers_typography',
	'label'       => esc_html__( 'Headers Typography', 'q-theme' ),
	'description' => esc_html__( 'Edit the font-family and font-weight of your document\'s headers.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'priority'    => 20,
	'default'     => [
		'font-family' => 'Noto Serif',
		'variant'     => '700',
	],
	'transport'   => 'auto',
	'output'      => [
		[
			'element' => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,.site-title',
		],
		[
			'context' => [ 'editor' ],
			'element' => [
				'.editor-post-title__block .editor-post-title__input',
				'.wp-block-heading h1',
				'.wp-block-heading h2',
				'.wp-block-heading h3',
				'.wp-block-heading h4',
				'.wp-block-heading h5',
				'.wp-block-heading h6',
			],
		],
	],
	'choices'     => [
		'fonts' => [
			'google' => [ 'popularity' ],
		],
	],
] );

/**
 * Focus on content-background control.
 */
q_theme_add_customizer_field( [
	'settings' => 'q_theme_logo_focus_on_content_background_control',
	'type'     => 'custom',
	'label'    => 'Looking for the text-color and typography options?',
	'section'  => 'q_theme_typography',
	'priority' => 25,
	'default'  => '<div style="margin-bottom:1em;"><button class="button-q-focus global-focus button button-primary button-large" data-context="control" data-focus="q_theme_grid_content_background_color">' . esc_html__( 'Click here to edit the content background-color', 'q-theme' ) . '</button></div>',
] );

q_theme_add_customizer_field( [
	'type'        => 'kirki-wcag-tc',
	'settings'    => 'q_theme_text_color',
	'label'       => esc_attr__( 'Text Color', 'q-theme' ),
	'description' => q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
	'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
	'section'     => 'q_theme_typography',
	'priority'    => 30,
	'default'     => '#000000',
	'css_vars'    => '--q-text-color',
	'transport'   => 'postMessage',
	'choices'     => [
		'setting' => 'q_theme_grid_content_background_color',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'kirki-wcag-tc',
	'settings'    => 'q_theme_headers_color',
	'label'       => esc_attr__( 'Headers Color', 'q-theme' ),
	'description' => q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
	'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
	'section'     => 'q_theme_typography',
	'default'     => '#000000',
	'css_vars'    => '--q-headers-color',
	'transport'   => 'postMessage',
	'priority'    => 40,
	'choices'     => [
		'setting' => 'q_theme_grid_content_background_color',
	],
] );

q_theme_add_customizer_field( [
	'settings'    => 'q_theme_links_color',
	'type'        => 'kirki-wcag-lc',
	'label'       => esc_attr__( 'Links Color', 'q-theme' ),
	'description' => esc_html__( 'Select the color for your links.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'transport'   => 'postMessage',
	'priority'    => 50,
	'choices'     => [
		'alpha' => false,
	],
	'default'     => '#0f5e97',
	'choices'     => [
		'backgroundColor' => 'q_theme_grid_content_background_color',
		'textColor'       => 'q_theme_text_color',
	],
	'css_vars'    => '--q-links-color',
] );

q_theme_add_customizer_field( [
	'settings'    => 'q_theme_links_hover_color',
	'type'        => 'kirki-wcag-lc',
	'label'       => esc_attr__( 'Links Hover Color', 'q-theme' ),
	'description' => esc_html__( 'Select the colors for your links on hover.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'transport'   => 'postMessage',
	'priority'    => 60,
	'choices'     => [
		'alpha' => false,
	],
	'default'     => '#541cfc',
	'css_vars'    => '--q-links-hover-color',
	'choices'     => [
		'backgroundColor' => 'q_theme_grid_content_background_color',
		'textColor'       => 'q_theme_text_color',
	],
] );

/**
 * Body typography.
 */
q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_body_font_size',
	'label'       => esc_html__( 'Body Font-Size', 'q-theme' ),
	'description' => esc_html__( 'We recommend you a font-size greater than 18px to ensure greater readability.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'default'     => 18,
	'priority'    => 70,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-font-size', '$px' ],
	'choices'     => [
		'min'    => 13,
		'max'    => 40,
		'step'   => 1,
		'suffix' => 'px',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_fluid_typography_ratio',
	'label'       => esc_html__( 'Fluid typography Ratio', 'q-theme' ),
	'description' => esc_html__( 'Larger values will increase the font-size more on bigger screens. Set to 0 if you don\'t want the font-size to change depending on the screen-size.', 'q-theme' ),
	/* translators: "Read this article" link. */
	'tooltip'     => sprintf( esc_html__( 'Need more Information? %s', 'q-theme' ), '<a href="https://wplemon.com/documentation/q-theme/typography/fluid-responsive-typography/" target="_blank">' . esc_html__( 'Read this article.', 'q-theme' ) ),
	'section'     => 'q_theme_typography',
	'default'     => 0.25,
	'priority'    => 80,
	'transport'   => 'postMessage',
	'css_vars'    => '--q-typo-ratio',
	'choices'     => [
		'min'  => 0,
		'max'  => 1,
		'step' => .001,
	],
] );

/**
 * Type Scale
 */
q_theme_add_customizer_field( [
	'settings'    => 'q_theme_type_scale_preset',
	'type'        => 'radio',
	'label'       => esc_attr__( 'Headers Size Scale', 'q-theme' ),
	'description' => esc_attr__( 'Controls the size relations between your headers and your main typography font-size.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'default'     => '1.333',
	'choices'     => [
		/* Translators: Numeric representation of the scale. */
		'1.067'  => sprintf( esc_attr__( '%s - Minor Second', 'q-theme' ), '1.067' ),
		/* Translators: Numeric representation of the scale. */
		'1.125'  => sprintf( esc_attr__( '%s - Major Second', 'q-theme' ), '1.125' ),
		/* Translators: Numeric representation of the scale. */
		'1.25'   => sprintf( esc_attr__( '%s - Major Third', 'q-theme' ), '1.250' ),
		/* Translators: Numeric representation of the scale. */
		'1.333'  => sprintf( esc_attr__( '%s - Perfect Fourth', 'q-theme' ), '1.333' ),
		/* Translators: Numeric representation of the scale. */
		'1.444'  => sprintf( esc_attr__( '%s - Augmented Fourth', 'q-theme' ), '1.414' ),
		/* Translators: Numeric representation of the scale. */
		'1.5'    => sprintf( esc_attr__( '%s - Perfect Fifth', 'q-theme' ), '1.500' ),
		/* Translators: Numeric representation of the scale. */
		'1.618'  => sprintf( esc_attr__( '%s - Golden Ratio', 'q-theme' ), '1.618' ),
		// Custom.
		'custom' => 'custom',
	],
	'transport'   => 'postMessage',
	'priority'    => 90,
	'preset'      => [
		'1.067' => [
			'settings' => [
				'q_theme_type_scale' => 1.067,
			],
		],
		'1.125' => [
			'settings' => [
				'q_theme_type_scale' => 1.125,
			],
		],
		'1.25'  => [
			'settings' => [
				'q_theme_type_scale' => 1.25,
			],
		],
		'1.333' => [
			'settings' => [
				'q_theme_type_scale' => 1.333,
			],
		],
		'1.444' => [
			'settings' => [
				'q_theme_type_scale' => 1.444,
			],
		],
		'1.5'   => [
			'settings' => [
				'q_theme_type_scale' => 1.5,
			],
		],
		'1.618' => [
			'settings' => [
				'q_theme_type_scale' => 1.618,
			],
		],
	],
] );

/**
 * Type Scale
 */
q_theme_add_customizer_field( [
	'settings'        => 'q_theme_type_scale',
	'type'            => 'slider',
	'label'           => esc_attr__( 'Headers Size Scale', 'q-theme' ),
	'description'     => esc_attr__( 'Controls the size relations between your headers and your main typography font-size.', 'q-theme' ),
	'section'         => 'q_theme_typography',
	'default'         => '1.333',
	'choices'         => [
		'min'  => 1,
		'max'  => 2,
		'step' => 0.001,
	],
	'transport'       => 'postMessage',
	'css_vars'        => '--q-typo-scale',
	'priority'        => 100,
	'active_callback' => [
		[
			'setting'  => 'q_theme_type_scale_preset',
			'operator' => '===',
			'value'    => 'custom',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'    => 'q_theme_links_decoration',
	'type'        => 'radio',
	'label'       => esc_attr__( 'Links decoration', 'q-theme' ),
	'description' => esc_html__( 'Select if you want links to be underlined or not. For increased accessibility it is recommended to underline links as this offers a more clear visual queue.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'default'     => 'underline',
	'priority'    => 110,
	'css_vars'    => '--q-links-text-decoration',
	'transport'   => 'postMessage',
	'choices'     => [
		'none'      => esc_attr__( 'None', 'q-theme' ),
		'underline' => esc_attr__( 'Underline', 'q-theme' ),
	],
] );

q_theme_add_customizer_field( [
	'settings'    => 'q_theme_header_links_decoration',
	'type'        => 'radio',
	'label'       => esc_attr__( 'Header Links Decoration', 'q-theme' ),
	'description' => esc_html__( 'Select if you want links inside headers to be underlined or not.', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'default'     => 'none',
	'priority'    => 120,
	'css_vars'    => '--q-header-links-text-decoration',
	'transport'   => 'postMessage',
	'choices'     => [
		'none'      => esc_attr__( 'None', 'q-theme' ),
		'underline' => esc_attr__( 'Underline', 'q-theme' ),
	],
] );

/*
// WIP - External link icons.
q_theme_add_customizer_field( [
	'settings'    => 'q_theme_link_external_icon',
	'type'        => 'switch',
	'label'       => esc_attr__( 'Add icon to indicate external links', 'q-theme' ),
	'description' => esc_html__( 'Accessibility improvement', 'q-theme' ),
	'section'     => 'q_theme_typography',
	'default'     => false,
] );
*/
