<?php
/**
 * Template part for Layer Slider
 *
 * @package Q Theme
 * @since 1.0
 */

?>
<div class="q-tp-revolution-slider q-tp">
	<?php

	// Get the selected slider.
	$slide = get_theme_mod( 'q_theme_grid_layerslider_slider', '' );

	// Print the slider.
	if ( $slide && function_exists( 'layerslider' ) ) {
		layerslider( $slide );
	}
	?>
</div>
