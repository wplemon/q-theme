<?php
/**
 * Init Later_Slider.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Layer_Slider;

if ( ! class_exists( 'LS_Sliders' ) ) {
	return;
}

new Layer_Slider();
