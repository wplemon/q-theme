<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

// Add section.
q_theme_add_customizer_section( 'q_theme_grid_part_details_footer_copyright', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Copyright Area', 'q-theme' ) ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/footer/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'section'     => 'q_theme_grid',
] );

q_theme_add_customizer_field( [
	'type'        => 'color',
	'settings'    => 'q_theme_grid_footer_copyright_bg_color',
	'label'       => esc_attr__( 'Copyright area background-color', 'q-theme' ),
	'description' => esc_html__( 'The background color for the copyright area.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_footer_copyright',
	'default'     => '#ffffff',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-footer-copyright-bg',
	'priority'    => 110,
	'choices'     => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'kirki-wcag-tc',
	'settings'    => 'q_theme_grid_footer_copyright_color',
	'label'       => esc_attr__( 'Copyright Text Color', 'q-theme' ),
	'description' => esc_html__( 'The main text color used for the copyright text. Please choose a color with sufficient contrast with the selected background-color.', 'q-theme' ) . '<br>' . q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
	'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
	'section'     => 'q_theme_grid_part_details_footer_copyright',
	'default'     => '#000000',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-footer-copyright-color',
	'priority'    => 120,
	'choices'     => [
		'setting' => 'q_theme_grid_footer_copyright_bg_color',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'kirki-wcag-lc',
	'settings'    => 'q_theme_grid_footer_copyright_links_color',
	'label'       => esc_attr__( 'Copyright Links Color', 'q-theme' ),
	'description' => esc_html__( 'The color of any links included in the copyright area.', 'q-theme' ),
	'tooltip'     => esc_html__( 'Hover colors are calculated based on the links hue difference setting (see the "Links & Buttons Styling" section).', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_footer_copyright',
	'default'     => '#0f5e97',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-footer-copyright-links-color',
	'priority'    => 130,
	'choices'     => [
		'backgroundColor' => 'q_theme_grid_footer_copyright_bg_color',
		'textColor'       => 'q_theme_grid_footer_copyright_color',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_footer_copyright_text_font_size',
	'label'       => esc_attr__( 'Copyright Text Font-Size', 'q-theme' ),
	'description' => esc_html__( 'Font-size is relative to the body font-size.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_footer_copyright',
	'default'     => 1,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-footer-copyright-font-size', '$em' ],
	'priority'    => 140,
	'choices'     => [
		'min'  => .5,
		'max'  => 2,
		'step' => .01,
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'radio-buttonset',
	'settings'    => 'q_theme_grid_footer_copyright_text_align',
	'label'       => esc_attr__( 'Copyright Text-Alignment', 'q-theme' ),
	'description' => esc_html__( 'Select if the copyright text will be aligned to the left, center or right.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_footer_copyright',
	'default'     => 'left',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-footer-copyright-text-align',
	'priority'    => 150,
	'choices'     => [
		'left'   => esc_attr__( 'Left', 'q-theme' ),
		'center' => esc_attr__( 'Center', 'q-theme' ),
		'right'  => esc_attr__( 'Right', 'q-theme' ),
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'code',
	'settings'        => 'q_theme_copyright_text',
	'label'           => esc_attr__( 'Copyright Text', 'q-theme' ),
	'description'     => esc_html__( 'The text that will be shown below the footer, on the lower part of your page (accepts HTML).', 'q-theme' ),
	'tooltip'         => esc_html__( 'If you want to completely hide the copyright area you can delete the contents of this setting.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_footer_copyright',
	/* translators: 1: CMS name, i.e. WordPress. 2: Theme name, 3: Theme author. */
	'default'         => sprintf( __( 'Proudly powered by %1$s | Theme: %2$s by %3$s.', 'q-theme' ), '<a href="https://wordpress.org/">WordPress</a>', 'Q', '<a href="https://wplemon.com/">wplemon.com</a>' ),
	'transport'       => 'postMessage',
	'choices'         => [
		'language' => 'html',
	],
	'priority'        => 160,
	'partial_refresh' => [
		'q_theme_the_grid_part_footer_copyright_template' => [
			'selector'            => '.q-tp-footer_copyright',
			'container_inclusive' => false,
			'render_callback'     => function() {
				get_template_part( 'grid-parts/footer/template-footer-copyright' );
			},
		],
	],
] );
