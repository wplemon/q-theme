<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Footer;
use Q_Theme\Customizer\Control\Color_A11y;

// Add sections & settings for widget areas.
$sidebars_nr = Footer::get_number_of_sidebars();
for ( $i = 1; $i <= $sidebars_nr; $i++ ) {
	q_theme_add_footer_widget_area_options( $i );
}

/**
 * Adds options for a footer widget-area.
 *
 * @since 1.0
 * @param int $id - The sidebar number.
 * @return void
 */
function q_theme_add_footer_widget_area_options( $id ) {

	// Add section.
	q_theme_add_customizer_section( "q_theme_grid_part_details_footer_sidebar_$id", [
		'title'       => sprintf(
			/* translators: The grid-part label. */
			esc_attr__( '%s Options', 'q-theme' ),
			/* translators: The number of the footer widget area. */
			sprintf( esc_html__( 'Footer Sidebar %d', 'q-theme' ), absint( $id ) )
		),
		'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/footer/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
		'section'     => 'q_theme_grid',
	] );

	// Background Color.
	q_theme_add_customizer_field( [
		'type'        => 'color',
		'settings'    => "q_theme_grid_footer_sidebar_{$id}_bg_color",
		'label'       => esc_attr__( 'Background Color', 'q-theme' ),
		'description' => esc_html__( 'Choose a background color for this widget-area.', 'q-theme' ),
		'section'     => "q_theme_grid_part_details_footer_sidebar_$id",
		'default'     => '#ffffff',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-footer-sidebar-$id-bg",
		'choices'     => [
			'alpha' => true,
		],
	] );

	// Text Color.
	q_theme_add_customizer_field( [
		'type'        => 'kirki-wcag-tc',
		'settings'    => "q_theme_grid_footer_sidebar_{$id}_color",
		'label'       => esc_attr__( 'Text Color', 'q-theme' ),
		'description' => esc_html__( 'The main text color used for the footer text.', 'q-theme' ) . '<br>' . q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
		'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
		'section'     => "q_theme_grid_part_details_footer_sidebar_$id",
		'default'     => '#000000',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-footer-sidebar-$id-color",
		'choices'     => [
			'setting' => "q_theme_grid_footer_sidebar_{$id}_bg_color",
		],
	] );

	// Links Color.
	q_theme_add_customizer_field( [
		'type'        => 'kirki-wcag-lc',
		'settings'    => "q_theme_grid_footer_sidebar_{$id}_links_color",
		'label'       => esc_attr__( 'Links Color', 'q-theme' ),
		'description' => esc_html__( 'The color for links in this widget-area.', 'q-theme' ),
		'section'     => "q_theme_grid_part_details_footer_sidebar_$id",
		'default'     => '#0f5e97',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-footer-sidebar-$id-links-color",
		'priority'    => 20,
		'choices'     => [
			'backgroundColor' => "q_theme_grid_footer_sidebar_{$id}_bg_color",
			'textColor'       => "q_theme_grid_footer_sidebar_{$id}_color",
		],
	] );
}
