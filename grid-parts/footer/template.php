<?php
/**
 * Template part for the footer.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Grid;
use Q_Theme\Grid_Part\Footer;
use Q_Theme\Style;

// Get the grid settings.
$settings = Grid::get_options( 'q_theme_footer_grid', Footer::get_grid_defaults() );

$style = new Style();

// Add the main grid styles.
$style->add_string( Grid::get_styles_responsive( [
	'large'      => Grid::get_options( 'q_theme_footer_grid' ),
	'small'      => Grid::get_options( 'q_theme_grid_footer_mobile' ),
	'breakpoint' => get_theme_mod( 'q_mobile_breakpoint', '800px' ),
	'selector'   => '.q-tp-footer > .inner',
	'prefix'     => true,
] ) );

// Add the stylesheet.
$style->add_file( get_theme_file_path( 'grid-parts/footer/styles/default.min.css' ) );

// Add css-vars.
$style->add_vars( [
	'--q-footer-bg'               => get_theme_mod( 'q_theme_grid_footer_background_color', '#ffffff' ),
	'--q-footer-border-top-width' => get_theme_mod( 'q_theme_grid_footer_border_top_width', 1 ) . 'px',
	'--q-footer-border-top-color' => get_theme_mod( 'q_theme_grid_footer_border_top_color', 'rgba(0,0,0,.1)' ),
	'--q-footer-max-width'        => get_theme_mod( 'q_theme_grid_footer_max_width', '' ),
] );
?>

<div class="q-tp q-tp-footer">
	<?php
	/**
	 * Print the styles.
	 */
	$style->the_css( 'q-inline-css-footer' );
	?>
	<div class="inner">
		<?php
		if ( isset( $settings['areas'] ) ) {
			foreach ( array_keys( $settings['areas'] ) as $part ) {

				if ( 0 === strpos( $part, 'footer_sidebar_' ) ) {
					/**
					 * Footer Sidebars.
					 * We use include( get_theme_file_path() ) here 
					 * because we need to pass the $sidebar_id var to the template.
					 */
					$sidebar_id = (int) str_replace( 'footer_sidebar_', '', $part );
					include get_theme_file_path( 'grid-parts/footer/template-footer-sidebar.php' );
					
				} elseif ( 'footer_social_media' === $part ) {
					/**
					 * Social Media.
					 */
					get_template_part( 'grid-parts/footer/template-social-media' );

				} elseif ( 'footer_copyright' === $part ) {
					/**
					 * Copyright.
					 */
					get_template_part( 'grid-parts/footer/template-footer-copyright' );

				} else {
					do_action( 'q_theme_the_grid_part', $part );
				}
			}
		}
		?>
	</div>
</div>
