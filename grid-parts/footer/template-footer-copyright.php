<?php
/**
 * Template part for the footer copyright.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Style;
// Add styles.
$styles = new Style();
$styles->add_vars( [
	'--q-footer-padding'               => get_theme_mod( 'q_theme_grid_footer_padding', '1em' ),
	'--q-footer-copyright-bg'          => get_theme_mod( 'q_theme_grid_footer_copyright_bg_color', '#ffffff' ),
	'--q-footer-copyright-color'       => get_theme_mod( 'q_theme_grid_footer_copyright_color', '#000000' ),
	'--q-footer-copyright-font-size'   => get_theme_mod( 'q_theme_grid_footer_copyright_text_font_size', 1 ) . 'em',
	'--q-footer-copyright-text-align'  => get_theme_mod( 'q_theme_grid_footer_copyright_text_align', 'left' ),
	'--q-footer-copyright-links-color' => get_theme_mod( 'q_theme_grid_footer_copyright_links_color', '#0f5e97' ),
] );
$styles->add_file( get_theme_file_path( 'grid-parts/footer/styles/copyright.min.css' ) );
$styles->the_css( 'q-inline-css-footer-copyright' );

?>

<div class="q-tp q-tp-footer_copyright">
	<div class="site-info">
		<div class="site-info-text">
			<?php
			echo get_theme_mod( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				'q_theme_copyright_text',
				sprintf(
					/* translators: 1: CMS name, i.e. WordPress. 2: Theme name, 3: Theme author. */
					__( 'Proudly powered by %1$s | Theme: %2$s by %3$s.', 'q-theme' ),
					'<a href="https://wordpress.org/">WordPress</a>',
					'Q',
					'<a href="https://wplemon.com/">wplemon.com</a>'
				)
			);
			?>
		</div>
	</div>
</div>
