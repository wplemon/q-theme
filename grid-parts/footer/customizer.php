<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Customizer;
use Q_Theme\Customizer\Control\Color_A11y;
use Q_Theme\Grid_Part\Footer;

q_theme_add_customizer_section( 'q_theme_grid_part_details_footer', [
	'title'       => esc_html__( 'Footer', 'q-theme' ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/footer/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 25,
] );

/**
 * Global toggle for mobile/desktop grid.
 */
q_theme_add_customizer_field( [
	'settings'          => 'q_theme_footer_grid_toggle',
	'type'              => 'radio-buttonset',
	'label'             => esc_html__( 'Toggle between desktop & mobile grids.', 'q-theme' ),
	'section'           => 'q_theme_grid_part_details_footer',
	'default'           => 'desktop',
	'priority'          => -10,
	'transport'         => 'postMessage',
	'choices'           => [
		'desktop' => esc_html__( 'Desktop', 'q-theme' ),
		'mobile'  => esc_html__( 'Mobile', 'q-theme' ),
	],
	'sanitize_callback' => function() {
		return 'desktop';
	},
] );

$footer_grid_parts = [
	[
		'label'    => esc_html__( 'Copyright Area', 'q-theme' ),
		'color'    => '#DC3232',
		'priority' => 100,
		'hidden'   => false,
		'id'       => 'footer_copyright',
	],
	[
		'label'    => esc_html__( 'Social Media', 'q-theme' ),
		'color'    => '#0996c3',
		'priority' => 200,
		'hidden'   => false,
		'id'       => 'footer_social_media',
	],
];

$sidebars_nr = Footer::get_number_of_sidebars();
for ( $i = 1; $i <= $sidebars_nr; $i++ ) {
	$footer_grid_parts[] = [
		/* translators: The widget-area number. */
		'label'    => sprintf( esc_html__( 'Footer Widget Area %d', 'q-theme' ), absint( $i ) ),
		'color'    => '#000000',
		'priority' => 8 + $i * 2,
		'hidden'   => false,
		'class'    => "footer_sidebar_$i",
		'id'       => "footer_sidebar_$i",
	];
}

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_footer_grid',
	'section'           => 'q_theme_grid_part_details_footer',
	'type'              => 'q_theme_grid',
	'grid-part'         => 'footer',
	'label'             => esc_html__( 'Grid Settings', 'q-theme' ),
	'description'       => sprintf(
		/* translators: Link attributes. */
		__( 'Edit settings for the grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
	),
	'default'           => Footer::get_grid_defaults(),
	'choices'           => [
		'parts'     => $footer_grid_parts,
		'duplicate' => 'q_theme_grid_footer_mobile',
	],
	'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
	'transport'         => 'postMessage',
	'partial_refresh'   => [
		'q_theme_footer_grid_template' => [
			'selector'            => '.q-tp-footer',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'footer' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_copy_footer_grid_to_mobile',
	'type'            => 'custom',
	'label'           => esc_html__( 'Copy desktop grid setings to mobile grid', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => '<div style="margin-bottom:1em;"><button class="button-q-copy-grid-setting button button-primary button-large" data-from="q_theme_footer_grid" data-to="q_theme_grid_footer_mobile">' . esc_html__( 'Click here to copy settings from desktop grid', 'q-theme' ) . '</button></div>',
	'active_callback' => [
		[
			'setting'  => 'q_theme_footer_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_grid_footer_mobile',
	'section'           => 'q_theme_grid_part_details_footer',
	'type'              => 'q_theme_grid',
	'grid-part'         => 'footer',
	'label'             => esc_html__( 'Footer Mobile Grid Settings', 'q-theme' ),
	'description'       => sprintf(
		/* translators: Link attributes. */
		__( 'Edit settings for the footer grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
	),
	'default'           => Footer::get_grid_mobile_defaults(),
	'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
	'choices'           => [
		'parts'              => $footer_grid_parts,
		'duplicate'          => 'q_theme_footer_grid',
		'disablePartButtons' => [ 'edit' ],
	],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
	'transport'         => 'postMessage',
	'partial_refresh'   => [
		'q_theme_footer_grid_mobile_template' => [
			'selector'            => '.q-tp-footer',
			'container_inclusive' => false,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'footer' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_footer_grid_gap',
	'label'           => esc_attr__( 'Grid Gap', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'grid-gap-description' ),
	'tooltip'         => esc_html__( 'If you have a background-color or background-image defined for your footer, then these will be visible through these gaps which creates a unique appearance since each grid-part looks separate.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => '0',
	'css_vars'        => '--q-footer-grid-gap',
	'transport'       => 'postMessage',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_footer_max_width',
	'label'           => esc_attr__( 'Max-Width', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'grid-part-max-width' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => '',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-footer-max-width',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_footer_padding',
	'label'           => esc_attr__( 'Padding', 'q-theme' ),
	'description'     => esc_html__( 'Inner padding for all parts in the footer.', 'q-theme' ),
	'tooltip'         => q_theme()->customizer->get_text( 'padding' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => '1em',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-footer-padding',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'color',
	'settings'        => 'q_theme_grid_footer_background_color',
	'label'           => esc_attr__( 'Background Color', 'q-theme' ),
	'description'     => esc_html__( 'Choose a background color for the footer.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => '#ffffff',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-footer-bg',
	'choices'         => [
		'alpha' => true,
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'slider',
	'settings'        => 'q_theme_grid_footer_border_top_width',
	'label'           => esc_attr__( 'Border-Top Width', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => 1,
	'transport'       => 'postMessage',
	'css_vars'        => [ '--q-footer-border-top-width', '$px' ],
	'priority'        => 50,
	'choices'         => [
		'min'    => 0,
		'max'    => 30,
		'step'   => 1,
		'suffix' => 'px',
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'color',
	'settings'        => 'q_theme_grid_footer_border_top_color',
	'label'           => esc_attr__( 'Border-top Color', 'q-theme' ),
	'description'     => esc_html__( 'Choose a color for the top border.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_footer',
	'default'         => 'rgba(0,0,0,.1)',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-footer-border-top-color',
	'choices'         => [
		'alpha' => true,
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );
