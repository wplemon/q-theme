<?php
/**
 * Template part for the footer sidebars.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Style;

$style = new Style();

// Add stylesheet.
$style->add_file( get_theme_file_path( 'grid-parts/footer/styles/sidebar.min.css' ) );

// Replace "ID" in the stylesheet.
$style->replace( 'ID', $sidebar_id );

// Add CSS-vars to replace.
$style->add_vars( [
	"--q-footer-sidebar-$sidebar_id-bg"          => get_theme_mod( "q_theme_grid_footer_sidebar_{$sidebar_id}_bg_color", '#ffffff' ),
	"--q-footer-sidebar-$sidebar_id-color"       => get_theme_mod( "q_theme_grid_footer_sidebar_{$sidebar_id}_color", '#000000' ),
	"--q-footer-sidebar-$sidebar_id-links-color" => get_theme_mod( "q_theme_grid_footer_sidebar_{$sidebar_id}_links_color", '#0f5e97' ),
	'--q-footer-padding'                         => get_theme_mod( 'q_theme_grid_footer_padding', '1em' ),
] );
?>

<div class="q-tp q-tp-footer_sidebar_<?php echo esc_attr( $sidebar_id ); ?>">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-sidebar-' . $sidebar_id );

	/**
	 * Print the sidebar.
	 */
	dynamic_sidebar( "footer_sidebar_{$sidebar_id}" );
	?>
</div>
