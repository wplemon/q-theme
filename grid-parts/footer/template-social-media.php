<?php
/**
 * Template part for the Footer Social-Media.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Grid_Part\Footer;
use Q_Theme\Style;

$setting = get_theme_mod( 'q_theme_grid_part_details_footer_social_icons', Footer::get_social_icons_default_value() );
if ( ! function_exists( 'q_theme_social_icons_svg' ) ) {

	// Include Social Icons Definitions.
	require_once get_template_directory() . '/inc/social-icons.php';
}
$icons = q_theme_social_icons_svg();

// Init Style class.
$style = new Style();

// Add CSS-vars.
$style->add_vars( [
	'--qftsocmedbg'                      => get_theme_mod( 'q_theme_grid_part_details_footer_social_icons_background_color', '#ffffff' ),
	'--q-footer-social-icons-text-align' => get_theme_mod( 'q_theme_grid_part_details_footer_social_icons_icons_text_align', 'right' ),
	'--q-footer-social-icons-size'       => get_theme_mod( 'q_theme_grid_part_details_footer_social_icons_size', 1 ) . 'em',
	'--q-footer-social-icons-padding'    => get_theme_mod( 'q_theme_grid_part_details_footer_social_icons_padding', .5 ) . 'em',
	'--q-footer-social-icons-color'      => get_theme_mod( 'q_theme_grid_part_details_footer_social_icons_icons_color', '#000000' ),
] );

// Add stylesheet.
$style->add_file( get_theme_file_path( 'grid-parts/footer/styles/social-icons.min.css' ) );
?>

<div class="q-tp q-tp-footer_social_media">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-footer-social-icons' );

	foreach ( $setting as $icon ) {
		if ( ! empty( $icon['url'] ) && ! empty( $icon['icon'] ) && isset( $icons[ $icon['icon'] ] ) ) {
			$url = ( 'mail' === $icon['icon'] ) ? 'mailto:' . antispambot( $icon['url'] ) : esc_url_raw( $icon['url'] );

			// The $url here is not escaped because it may contain a "mailto:" link instead of an actual URL.
			// However, the line right above this one escapes this properly.
			echo '<a href="' . $url . '" target="_blank" rel="noopener" title="' . esc_attr( $icon['icon'] ) . '">'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

			// The icons here are hardcoded. There's no need to escape them, and escaping <svg> icons on each page-load 
			// would be resources-intensive and an overkill.
			echo $icons[ $icon['icon'] ]; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

			// Close the link.
			echo '</a>';
		}
	}
	?>
</div>
