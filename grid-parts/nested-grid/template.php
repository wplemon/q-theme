<?php
/**
 * Template part for the Nested Grids.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Grid;
use Q_Theme\Style;

$settings = Grid::get_options( "q_theme_nested_grid_$id" );
$classes  = [
	'q-tp',
	"q-tp-nested-grid-$id",
];
if ( get_theme_mod( "q_theme_nested_grid_{$id}_sticky", false ) ) {
	$classes[] = 'q-sticky';
}

$style = new Style();
$style->add_string( Grid::get_styles_responsive( [
	'large'      => $settings,
	'small'      => false,
	'breakpoint' => get_theme_mod( 'q_mobile_breakpoint', '800px' ),
	'selector'   => '.q-tp-nested-grid-1 > .inner',
	'prefix'     => true,
] ) );
$style->add_file( get_theme_file_path( 'grid-parts/nested-grid/styles/default.min.css' ) );

// Add styles for large screens.
$style->add_string( '@media only screen and (min-width:' . get_theme_mod( 'q_mobile_breakpoint', '800px' ) . '){' );
$style->add_file( get_theme_file_path( 'grid-parts/nested-grid/styles/large.min.css' ) );
$style->add_string( '}' );

// Generate grid styles for parts.
$style->add_string( Grid::get_styles( $settings, ".q-tp-nstgrd{$id} > .inner", true ) );
$style->replace( 'ID', $id );
$style->add_vars( [
	"q-nested-grid-$id-background" => get_theme_mod( "q_theme_nested_grid_{$id}_background_color", '#ffffff' ),
	"q-nested-grid-$id-padding"    => get_theme_mod( "q_theme_nested_grid_{$id}_padding", 0 ),
	"q-nested-grid-$id-box-shadow" => get_theme_mod( "q_theme_nested_grid_{$id}_box_shadow", 'none' ),
	"q-nested-grid-$id-max-width"  => get_theme_mod( "q_theme_nested_grid_{$id}_max_width", '' ),
	"q-nested-grid-$id-grid-gap"   => get_theme_mod( "q_theme_nested_grid_{$id}_gap", 0 ),
] );
?>
<div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-nested-grid-' . $id );
	?>
	<div class="inner">
		<?php if ( isset( $settings['areas'] ) ) : ?>
			<?php foreach ( array_keys( $settings['areas'] ) as $part ) : ?>
				<?php do_action( 'q_theme_the_grid_part', $part ); ?>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
