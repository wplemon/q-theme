<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Nested_Grid;
use Q_Theme\Grid_Parts;

/**
 * Add the nested-grid options.
 *
 * @since 1.0
 * @param int $id The nested-grid ID.
 */
function q_theme_add_nested_grid_options( $id ) {

	q_theme_add_customizer_section( "q_theme_grid_part_details_nested-grid-{$id}", [
		'title'       => sprintf(
			/* translators: The grid-part label. */
			esc_attr__( '%s Options', 'q-theme' ),
			/* translators: Number of a nested-grid. */
			sprintf( esc_html__( 'Nested Grid %d', 'q-theme' ), $id )
		),
		'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/nested-grid/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
		'section'     => 'q_theme_grid',
	] );

	$parts = Grid_Parts::get_instance()->get_parts();

	// Remove parts that are not valid in this sub-grid.
	foreach ( $parts as $key => $part ) {

		if ( isset( $part['id'] ) ) {

			// Remove content.
			if ( 'content' === $part['id'] ) {
				unset( $parts[ $key ] );
			}

			if ( "nested-grid-$id" === $part['id'] ) {
				unset( $parts[ $key ] );
			}
		}
	}

	q_theme_add_customizer_field( [
		'settings'          => "q_theme_nested_grid_$id",
		'section'           => "q_theme_grid_part_details_nested-grid-$id",
		'type'              => 'q_theme_grid',
		'grid-part'         => "nested-grid-$id",
		'label'             => esc_html__( 'Grid Settings', 'q-theme' ),
		'description'       => sprintf(
			/* translators: Link attributes. */
			__( 'Edit settings for the grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
		),
		'default'           => [
			'rows'         => 1,
			'columns'      => 2,
			'areas'        => [],
			'gridTemplate' => [
				'rows'    => [],
				'columns' => [],
			],
		],
		'choices'           => [
			'parts' => $parts,
		],
		'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
		'transport'         => 'postMessage',
		'partial_refresh'   => [
			"q_theme_nested_grid_{$id}_template" => [
				'selector'            => ".q-tp-nested-grid-{$id}",
				'container_inclusive' => true,
				'render_callback'     => function() {
					do_action( 'q_theme_the_grid_part', "nested-grid-{$id}" );
				},
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'dimension',
		'settings'    => "q_theme_nested_grid_{$id}_padding",
		'label'       => esc_attr__( 'Grid Container Padding', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_nested-grid-$id",
		'default'     => '0',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nested-grid-$id-padding",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'dimension',
		'settings'    => "q_theme_nested_grid_{$id}_gap",
		'label'       => esc_attr__( 'Grid Container Gap', 'q-theme' ),
		'description' => q_theme()->customizer->get_text( 'grid-gap-description' ),
		'tooltip'     => q_theme()->customizer->get_text( 'grid-gap-tooltip' ),
		'section'     => "q_theme_grid_part_details_nested-grid-$id",
		'default'     => '0',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nested-grid-$id-grid-gap",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'dimension',
		'settings'    => "q_theme_nested_grid_{$id}_max_width",
		'label'       => esc_attr__( 'Grid Container max-width', 'q-theme' ),
		'description' => esc_html__( 'The maximum width for this grid.', 'q-theme' ),
		'tooltip'     => esc_html__( 'By setting the max-width to something other than 100% you get a boxed layout.', 'q-theme' ),
		'section'     => "q_theme_grid_part_details_nested-grid-$id",
		'default'     => '',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nested-grid-$id-max-width",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'color',
		'settings'    => "q_theme_nested_grid_{$id}_background_color",
		'label'       => esc_attr__( 'Grid Container background-color', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_nested-grid-$id",
		'default'     => '#ffffff',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nested-grid-$id-bg",
		'choices'     => [
			'alpha' => true,
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'radio',
		'settings'    => "q_theme_nested_grid_{$id}_box_shadow",
		'label'       => esc_html__( 'Drop Shadow Intensity', 'q-theme' ),
		'description' => esc_html__( 'Set to "None" if you want to disable the shadow for this grid-part, or increase the intensity for a more dramatic effect.', 'q-theme' ),
		'section'     => "q_theme_grid_part_details_nested-grid-$id",
		'default'     => 'none',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nested-grid-$id-box-shadow",
		'priority'    => 200,
		'choices'     => [
			'none' => esc_html__( 'None', 'q-theme' ),
			'0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)' => esc_html__( 'Extra Light', 'q-theme' ),
			'0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)' => esc_html__( 'Light', 'q-theme' ),
			'0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)' => esc_html__( 'Medium', 'q-theme' ),
			'0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)' => esc_html__( 'Heavy', 'q-theme' ),
			'0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)' => esc_html__( 'Extra Heavy', 'q-theme' ),
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'switch',
		'settings'    => "q_theme_nested_grid_{$id}_sticky",
		'label'       => esc_attr__( 'Sticky', 'q-theme' ),
		'description' => q_theme()->customizer->get_text( 'sticky-description' ),
		'tooltip'     => q_theme()->customizer->get_text( 'sticky-tooltip' ),
		'section'     => "q_theme_grid_part_details_nested-grid-$id",
		'default'     => false,
		'transport'   => 'auto',
		'priority'    => 300,
	] );
}

$number = Nested_Grid::get_number_of_nested_grids();
for ( $i = 1; $i <= $number; $i++ ) {
	q_theme_add_nested_grid_options( $i );
}
