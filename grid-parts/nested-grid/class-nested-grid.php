<?php
/**
 * Q Theme Nested_Grid grid-part
 *
 * @package Q Theme
 */

namespace Q_Theme\Grid_Part;

use Q_Theme\Grid;
use Q_Theme\Grid_Part;
use Q_Theme\Style;

/**
 * The Q_Theme\Grid_Part\Nested_Grid object.
 *
 * @since 1.0
 */
class Nested_Grid extends Grid_Part {

	/**
	 * An array of files to include.
	 *
	 * @access protected
	 * @since 1.0
	 * @var array
	 */
	protected $include_files = [
		'customizer.php',
	];

	/**
	 * The path to this directory..
	 *
	 * @access protected
	 * @since 1.0
	 * @var string
	 */
	protected $dir = __DIR__;

	/**
	 * Returns the grid-part definition.
	 *
	 * @access protected
	 * @since 1.0
	 * @return void
	 */
	protected function set_part() {}

	/**
	 * Hooks & extra operations.
	 *
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function init() {
		add_action( 'q_theme_the_grid_part', [ $this, 'render' ] );
	}

	/**
	 * Adds the grid-part to the array of grid-parts.
	 *
	 * @access public
	 * @since 1.0
	 * @param array $parts The existing grid-parts.
	 * @return array
	 */
	public function add_template_part( $parts ) {
		$number = self::get_number_of_nested_grids();
		for ( $i = 1; $i <= $number; $i++ ) {
			$parts[] = [
				/* translators: Number of a nested-grid. */
				'label'    => sprintf( esc_html__( 'Nested Grid %d', 'q-theme' ), absint( $i ) ),
				'color'    => '#000000',
				'priority' => 250,
				'class'    => "nstgrd{$i}",
				'id'       => "nested-grid-{$i}",
				'grid'     => "q_theme_nested_grid_{$i}",
			];
		}
		return $parts;
	}

	/**
	 * Render this grid-part.
	 * 
	 * @access public
	 * @since 1.0
	 * @param string $part The grid-part ID.
	 * @return void
	 */
	public function render( $part ) {
		if ( 0 === strpos( $part, 'nested-grid-' ) && is_numeric( str_replace( 'nested-grid-', '', $part ) ) ) {
			$id = (int) str_replace( 'nested-grid-', '', $part );
			/**
			 * We use include( get_theme_file_path() ) here 
			 * because we need to pass the $sidebar_id var to the template.
			 */
			include get_theme_file_path( 'grid-parts/nested-grid/template.php' );
		}
	}

	/**
	 * Gets the number of navigation menus.
	 * Returns the object's $number property.
	 *
	 * @static
	 * @access public
	 * @since 1.0
	 */
	public static function get_number_of_nested_grids() {
		return apply_filters( 'q_theme_get_number_of_nested_grids', 3 );
	}
}
