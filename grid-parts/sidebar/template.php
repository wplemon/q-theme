<?php
/**
 * Template part for the Sidebars.
 *
 * @package Q Theme
 * @since 1.0
 */

?>
<div class="q-tp q-tp-sidebar_<?php echo absint( $id ); ?>">
	<style>.q-tp-sidebar_<?php echo absint( $id ); ?>{flex-direction:column;justify-content:flex-start;}</style>
	<?php dynamic_sidebar( "sidebar-$id" ); ?>
</div>
