<?php
/**
 * Init Sidebar.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Sidebar;

new Sidebar();
