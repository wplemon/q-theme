<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Customizer\Control\Color_A11y;
use Q_Theme\Grid_Part\Sidebar;
use Q_Theme\AMP;

$number = Sidebar::get_number_of_sidebars();
for ( $i = 1; $i <= $number; $i++ ) {
	q_theme_sidebar_customizer_options( $i );
}

/**
 * This function creates all options for a sidebar.
 * We use a parameter since we'll allow multiple sidebars.
 *
 * @since 1.0
 * @param int $id The number of this sidebar.
 * @return void
 */
function q_theme_sidebar_customizer_options( $id ) {

	/* translators: The number of the widget area. */
	$label = get_theme_mod( "q_theme_grid_widget_area_{$id}_name", sprintf( esc_html__( 'Widget Area %d', 'q-theme' ), intval( $id ) ) );

	q_theme_add_customizer_section( "q_theme_grid_part_details_sidebar_$id", [
		/* translators: The grid-part label. */
		'title'       => sprintf( esc_attr__( '%s Advanced Options', 'q-theme' ), $label ),
		'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/widget-area/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
		'section'     => 'q_theme_grid',
	] );

	/**
	 * Focus on widget-area.
	 */
	q_theme_add_customizer_field( [
		'settings' => "q_theme_sidebar_focus_on_sidebar_{$id}_section",
		'type'     => 'custom',
		'label'    => '',
		'section'  => "q_theme_grid_part_details_sidebar_$id",
		'default'  => '<div style="margin-bottom:1em;"><button class="button-q-focus global-focus button button-primary button-large" data-context="section" data-focus="sidebar-widgets-' . "sidebar-{$id}" . '">' . esc_html__( 'Click here to edit your widgets', 'q-theme' ) . '</button></div>',
	] );

	q_theme_add_customizer_field( [
		'type'        => 'color',
		'settings'    => "q_theme_grid_sidebar_{$id}_background_color",
		'label'       => esc_attr__( 'Background Color', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_sidebar_$id",
		'default'     => '#ffffff',
		'priority'    => 10,
		'transport'   => 'auto',
		'choices'     => [
			'alpha' => true,
		],
		'output'      => [
			[
				'element'  => ".q-tp-sidebar_{$id}",
				'property' => 'background-color',
			],
		],
		'css_vars'    => "--q-sidebar-{$id}-bg",
		'choices'     => [
			'alpha' => true,
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'kirki-wcag-tc',
		'settings'    => "q_theme_grid_sidebar_{$id}_color",
		'label'       => esc_attr__( 'Text Color', 'q-theme' ),
		'description' => q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
		'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
		'section'     => "q_theme_grid_part_details_sidebar_$id",
		'default'     => '#000000',
		'priority'    => 20,
		'transport'   => 'auto',
		'choices'     => [
			'setting' => "q_theme_grid_sidebar_{$id}_background_color",
		],
		'output'      => [
			[
				'element'  => ".q-tp-sidebar_{$id},.q-tp-sidebar_{$id} h1,.q-tp-sidebar_{$id} h2,.q-tp-sidebar_{$id} h3,.q-tp-sidebar_{$id} h4,.q-tp-sidebar_{$id} h5,.q-tp-sidebar_{$id} h6,.q-tp-sidebar_{$id} .widget-title",
				'property' => 'color',
			],
		],
		'css_vars'    => "--q-sidebar-{$id}-color",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'kirki-wcag-lc',
		'settings'    => "q_theme_grid_sidebar_{$id}_links_color",
		'label'       => esc_attr__( 'Links Color', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_sidebar_$id",
		'default'     => '#0f5e97',
		'priority'    => 30,
		'transport'   => 'auto',
		'choices'     => [
			'alpha' => true,
		],
		'output'      => [
			[
				'element'  => [
					".q-tp-sidebar_{$id} a",
					".q-tp-sidebar_{$id} a:visited",
					".q-tp-sidebar_{$id} a:hover",
					".q-tp-sidebar_{$id} a:focus",
					".q-tp-sidebar_{$id} a:visited:hover",
					".q-tp-sidebar_{$id} a:visited:focus",
				],
				'property' => 'color',
			],
		],
		'css_vars'    => "--q-sidebar-{$id}-links-color",
		'choices'     => [
			'backgroundColor' => "q_theme_grid_sidebar_{$id}_background_color",
			'textColor'       => "q_theme_grid_sidebar_{$id}_color",
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'text',
		'settings'    => "q_theme_grid_sidebar_{$id}_padding",
		'label'       => esc_attr__( 'Padding', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_sidebar_$id",
		'priority'    => 40,
		'default'     => '1em',
		'transport'   => 'auto',
		'output'      => [
			[
				'element'  => ".q-tp-sidebar_{$id}",
				'property' => 'padding',
			],
		],
		'css_vars'    => "--q-sidebar-{$id}-padding",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'text',
		'settings'    => "q_theme_grid_sidebar_{$id}_widgets_margin",
		'label'       => esc_attr__( 'Margin between widgets', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_sidebar_$id",
		'priority'    => 43,
		'default'     => '20px',
		'transport'   => 'auto',
		'output'      => [
			[
				'element'  => ".q-tp-sidebar_{$id} .widget",
				'property' => 'margin-bottom',
			],
		],
		'css_vars'    => "--q-sidebar-{$id}-margin",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'slider',
		'settings'    => "q_theme_grid_sidebar_{$id}_font_size",
		'label'       => esc_attr__( 'Font Size', 'q-theme' ),
		'description' => '',
		'section'     => "q_theme_grid_part_details_sidebar_$id",
		'priority'    => 47,
		'default'     => 1,
		'choices'     => [
			'min'  => 0.5,
			'max'  => 3,
			'step' => 0.01,
		],
		'transport'   => 'auto',
		'output'      => [
			[
				'element'       => ".q-tp-sidebar_{$id}",
				'property'      => 'font-size',
				'value_pattern' => '$em',
			],
		],
		'css_vars'    => [ "--q-sidebar-{$id}-font-size", '$em' ],
	] );
}
