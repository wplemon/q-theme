<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

q_theme_add_customizer_section( 'q_theme_grid_part_details_revolution-slider', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Revolution Slider', 'q-theme' ) ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/revolution-slider/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'section'     => 'q_theme_grid',
] );

$slider        = new \RevSliderSlider();
$sliders_array = [
	'' => esc_html__( 'Select Slider', 'q-theme' ),
];
foreach ( $slider->getArrSliders() as $slide ) {
	$sliders_array[ $slide->getAlias() ] = $slide->getTitle();
}

q_theme_add_customizer_field( [
	'type'            => 'select',
	'settings'        => 'q_theme_grid_revslider_slider',
	'label'           => esc_attr__( 'Select Slider', 'q-theme' ),
	'description'     => esc_html__( 'Select the slider you want to assign to this grid-part.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_revolution-slider',
	'default'         => '',
	'priority'        => 10,
	'transport'       => 'refresh',
	'choices'         => $sliders_array,
	'transport'       => 'postMessage',
	'partial_refresh' => [
		'q_theme_grid_revslider_slider_template' => [
			'selector'            => '.q-tp-revolution-slider',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'revolution-slider' );
			},
		],
	],
] );
