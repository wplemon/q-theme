<?php
/**
 * Init Revolution_Slider.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Revolution_Slider;

if ( ! class_exists( 'RevSliderSlider' ) ) {
	return;
}

new Revolution_Slider();
