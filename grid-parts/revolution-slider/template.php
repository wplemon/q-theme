<?php
/**
 * Template part for Revolution Slider
 *
 * @package Q Theme
 * @since 1.0
 */

?>
<div class="q-tp q-tp-revolution-slider">
	<?php
	
	// Get the selected slider.
	$slide = get_theme_mod( 'q_theme_grid_revslider_slider', '' );

	// Print the slider.
	if ( $slide ) {
		echo do_shortcode( '[rev_slider alias="' . esc_html( $slide ) . '"]' );
	}
	?>
</div>
