<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

q_theme_add_customizer_section( 'q_theme_grid_part_details_breadcrumbs', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Breadcrumbs', 'q-theme' ) ),
	'section'     => 'q_theme_grid',
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/breadcrumbs/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 20,
] );

q_theme_add_customizer_field( [
	'type'        => 'text',
	'settings'    => 'q_theme_grid_breadcrumbs_padding',
	'label'       => esc_attr__( 'Padding', 'q-theme' ),
	'description' => esc_html__( 'Inner padding for this grid-part. Use any valid CSS value.', 'q-theme' ),
	'tooltip'     => q_theme()->customizer->get_text( 'padding' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'default'     => '1em',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-breadcrumbs-padding',
] );

q_theme_add_customizer_field( [
	'type'        => 'dimension',
	'settings'    => 'q_theme_grid_breadcrumbs_max_width',
	'label'       => esc_attr__( 'Max-Width', 'q-theme' ),
	'description' => q_theme()->customizer->get_text( 'grid-part-max-width' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'default'     => '',
	'css_vars'    => '--q-breadcrumbs-max-width',
	'transport'   => 'postMessage',
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_breadcrumbs_font_size',
	'label'       => esc_attr__( 'Font Size', 'q-theme' ),
	'description' => esc_html__( 'Controls the font-size for your breadcrumbs.', 'q-theme' ),
	'tooltip'     => q_theme()->customizer->get_text( 'related-font-size' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'default'     => 1,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-breadcrumbs-font-size', '$em' ],
	'choices'     => [
		'min'    => .5,
		'max'    => 2,
		'step'   => .01,
		'suffix' => 'em',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_breadcrumbs_whitespace',
	'label'       => esc_attr__( 'Separators whitespace', 'q-theme' ),
	'description' => esc_html__( 'Controls the whitespace included in separators. Use this option if you want to make breadcrumb parts further apart, or closer together.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'default'     => 1,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-breadcrumbs-whitespace', '$em' ],
	'choices'     => [
		'min'    => 0,
		'max'    => 3,
		'step'   => .01,
		'suffix' => 'em',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'color',
	'settings'    => 'q_theme_grid_breadcrumbs_background_color',
	'label'       => esc_attr__( 'Background Color', 'q-theme' ),
	'description' => esc_html__( 'Set the background-color for this grid-part.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'default'     => '#ffffff',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-breadcrumbs-bg',
	'choices'     => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'kirki-wcag-tc',
	'settings'    => 'q_theme_grid_breadcrumbs_color',
	'label'       => esc_attr__( 'Text Color', 'q-theme' ),
	'description' => q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
	'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'css_vars'    => '--q-breadcrumbs-color',
	'default'     => '#000000',
	'transport'   => 'postMessage',
	'choices'     => [
		'setting' => 'q_theme_grid_breadcrumbs_background_color',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'radio',
	'settings'    => 'q_theme_grid_breadcrumbs_text_align',
	'label'       => esc_attr__( 'Alignment', 'q-theme' ),
	'description' => esc_html__( 'Select if you want your breadcrumbs aligned to the left, right, or centered.', 'q-theme' ),
	'tooltip'     => esc_html__( 'Please note that this option does not change the order of your breadcrumbs, only their alignment inside their container.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_breadcrumbs',
	'default'     => 'left',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-breadcrumbs-text-align',
	'choices'     => [
		'left'   => esc_html__( 'Left', 'q-theme' ),
		'center' => esc_html__( 'Center', 'q-theme' ),
		'right'  => esc_html__( 'Right', 'q-theme' ),
	],
] );
