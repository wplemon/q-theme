<?php
/**
 * Init Breadcrumbs.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Breadcrumbs;

new Breadcrumbs();
