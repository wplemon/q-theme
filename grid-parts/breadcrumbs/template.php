<?php
/**
 * Template part for the breadcrumbs.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Style;

// Early exit if we're on the frontpage.
if ( is_front_page() || is_home() ) {
	return;
}

// Add styles.
$style = new Style();
$style->add_file( get_theme_file_path( 'grid-parts/breadcrumbs/styles/styles.min.css' ) );
$style->add_vars( [
	'--q-breadcrumbs-bg'         => get_theme_mod( 'q_theme_grid_breadcrumbs_background_color', '#ffffff' ),
	'--q-breadcrumbs-font-size'  => get_theme_mod( 'q_theme_grid_breadcrumbs_font_size', 1 ) . 'em',
	'--q-breadcrumbs-color'      => get_theme_mod( 'q_theme_grid_breadcrumbs_color', '#000000' ),
	'--q-breadcrumbs-padding'    => get_theme_mod( 'q_theme_grid_breadcrumbs_padding', '1em' ),
	'--q-breadcrumbs-text-align' => get_theme_mod( 'q_theme_grid_breadcrumbs_text_align', 'left' ),
	'--q-breadcrumbs-whitespace' => get_theme_mod( 'q_theme_grid_breadcrumbs_whitespace', 1 ) . 'em',
	'--q-breadcrumbs-max-width'  => get_theme_mod( 'q_theme_grid_breadcrumbs_max_width', '100%' ),
] );
?>
<div class="q-tp q-tp-breadcrumbs">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-breadcrumbs' );
	?>
	<div class="inner">
		<?php
		// The breadcrumbs.
		\Hybrid\Breadcrumbs\Trail::display( apply_filters( 'q_theme_breadcrumbs_args', [
			'show_on_front' => false,
			'labels'        => [
				'title' => false,
			],
		] ) );
		?>
	</div>
</div>
