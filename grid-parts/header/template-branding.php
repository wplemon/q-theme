<?php
/**
 * Template part for the Header Branding.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Style;

$style = new Style();
$style->add_vars( [
	'--q-branding-bg'               => get_theme_mod( 'q_theme_grid_header_branding_background_color', '#ffffff' ),
	'--q-branding-horizontal-align' => get_theme_mod( 'q_theme_grid_header_branding_horizontal_align', 'left' ),
	'--q-branding-vertical-align'   => get_theme_mod( 'q_theme_grid_header_branding_vertical_align', 'center' ),
	'--q-branding-padding'          => get_theme_mod( 'q_theme_grid_header_branding_padding', '0.5em' ),
	'--q-branding-elements-padding' => get_theme_mod( 'q_theme_branding_inline_spacing', 1 ) . 'em',
	'--q-logo-max-width'            => get_theme_mod( 'q_theme_logo_max_width', 100 ) . 'px',
] );
$style->add_file( get_theme_file_path( 'grid-parts/header/styles/branding.min.css' ) );
$style->the_css( 'q-inline-css-header-branding' );
?>
<div class="q-tp q-tp-header_branding<?php echo ( has_custom_logo() ) ? ' has-logo' : ''; ?><?php echo get_theme_mod( 'q_theme_branding_inline', false ) ? ' inline' : ' vertical'; ?>">
	<?php if ( has_custom_logo() ) : ?>
		<?php the_custom_logo(); ?>
	<?php endif; ?>

	<?php if ( is_front_page() && is_home() ) : ?>
		<h1 class="site-title h3<?php echo ( ! display_header_text() ) ? ' hidden' : ''; ?>">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php bloginfo( 'name' ); ?>
			</a>
		</h1>
	<?php else : ?>
		<p class="site-title h3<?php echo ( ! display_header_text() ) ? ' hidden' : ''; ?>">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php bloginfo( 'name' ); ?>
			</a>
		</p>
	<?php endif; ?>
	<?php $description = get_bloginfo( 'description', 'display' ); ?>
	<?php if ( $description || is_customize_preview() ) : ?>
		<p class="site-description<?php echo ( ! display_header_text() ) ? ' hidden' : ''; ?>"><?php echo $description; /* WPCS: xss ok. */ ?></p>
	<?php endif; ?>
</div>
