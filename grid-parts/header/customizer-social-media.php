<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Header;

if ( ! function_exists( 'q_theme_social_icons_svg' ) ) {

	// Include Social Icons Definitions.
	require_once get_template_directory() . '/inc/social-icons.php';
}

q_theme_add_customizer_section( 'q_theme_grid_part_details_social_media', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Header Contact Info', 'q-theme' ) ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/header/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'section'     => 'q_theme_grid',
	'priority'    => 20,
] );

q_theme_add_customizer_field( [
	'type'            => 'repeater',
	'settings'        => 'q_theme_grid_part_details_social_icons',
	'label'           => esc_attr__( 'Social Media Links', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_social_media',
	'default'         => Header::social_icons_default_value(),
	'row_label'       => [
		'type'  => 'field',
		'field' => 'icon',
	],
	'button_label'    => esc_html__( 'Add Icon', 'q-theme' ),
	'fields'          => [
		'icon' => [
			'type'    => 'select',
			'label'   => esc_attr__( 'Select Icon', 'q-theme' ),
			'default' => '',
			'choices' => q_theme_social_icons_svg( 'keys_only' ),
		],
		'url'  => [
			'type'    => 'text',
			'label'   => esc_attr__( 'Link URL', 'q-theme' ),
			'default' => '',
		],
	],
	'transport'       => 'postMessage',
	'partial_refresh' => [
		'q_theme_grid_part_details_social_icons_template' => [
			'selector'            => '.q-tp-social_media',
			'container_inclusive' => false,
			'render_callback'     => function() {
				get_template_part( 'grid-parts/header/template-social-media' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_part_details_social_icons_size',
	'label'       => esc_attr__( 'Size', 'q-theme' ),
	'description' => esc_html__( 'Controls the size for social-media icons.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_social_media',
	'default'     => .85,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-header-social-icons-size', '$em' ],
	'choices'     => [
		'min'    => .3,
		'max'    => 3,
		'step'   => .01,
		'suffix' => 'em',
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_part_details_social_icons_padding',
	'label'       => esc_attr__( 'Padding', 'q-theme' ),
	'description' => esc_html__( 'Controls the padding for social-media icons.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_social_media',
	'default'     => .5,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-header-social-icons-padding', '$em' ],
	'choices'     => [
		'min'    => 0,
		'max'    => 2,
		'step'   => .01,
		'suffix' => 'em',
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'color',
	'settings'  => 'q_theme_grid_part_details_social_icons_background_color',
	'label'     => esc_attr__( 'Background Color', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_social_media',
	'default'   => '#ffffff',
	'transport' => 'postMessage',
	'css_vars'  => '--q-header-social-icons-bg',
	'choices'   => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'color',
	'settings'  => 'q_theme_grid_part_details_social_icons_icons_color',
	'label'     => esc_attr__( 'Icons Color', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_social_media',
	'default'   => '#000000',
	'transport' => 'postMessage',
	'css_vars'  => '--q-header-social-icons-color',
	'choices'   => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'radio-buttonset',
	'settings'    => 'q_theme_grid_part_details_social_icons_icons_text_align',
	'label'       => esc_attr__( 'Icons Alignment', 'q-theme' ),
	'description' => esc_html__( 'Select how the icons will be aligned.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_social_media',
	'default'     => 'right',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-header-social-icons-text-align',
	'choices'     => [
		'flex-start' => esc_html__( 'Left', 'q-theme' ),
		'center'     => esc_html__( 'Center', 'q-theme' ),
		'flex-end'   => esc_html__( 'Right', 'q-theme' ),
	],
] );
