<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

q_theme_add_customizer_section( 'q_theme_grid_part_details_header_branding', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_html__( '%s Options', 'q-theme' ), esc_html__( 'Site Branding', 'q-theme' ) ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/header/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'section'     => 'q_theme_grid',
] );

/**
 * Focus on title_tagline section.
 */
q_theme_add_customizer_field( [
	'settings' => 'q_theme_logo_focus_on_grid_part_section',
	'type'     => 'custom',
	'label'    => '',
	'section'  => 'title_tagline',
	'priority' => 1,
	'default'  => '<div style="margin-bottom:1em;"><button class="button-q-focus global-focus button button-primary button-large" data-context="section" data-focus="q_theme_grid_part_details_header_branding">' . esc_html__( 'Click here to edit the branding grid-part', 'q-theme' ) . '</button></div>',
] );

/**
 * Focus on title_tagline section.
 */
q_theme_add_customizer_field( [
	'settings' => 'q_theme_logo_focus_on_core_section',
	'type'     => 'custom',
	'label'    => '',
	'section'  => 'q_theme_grid_part_details_header_branding',
	'default'  => '<div style="margin-bottom:1em;"><button class="button-q-focus global-focus button button-primary button-large" data-context="section" data-focus="title_tagline">' . esc_html__( 'Click here to edit your site identity', 'q-theme' ) . '</button></div>',
] );


q_theme_add_customizer_field( [
	'settings'        => 'q_theme_logo_max_width',
	'type'            => 'slider',
	'label'           => esc_html__( 'Logo Maximum Width', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header_branding',
	'default'         => 100,
	'choices'         => [
		'min'    => 10,
		'max'    => 600,
		'step'   => 1,
		'suffix' => 'px',
	],
	'transport'       => 'postMessage',
	'css_vars'        => [ '--q-logo-max-width', '$px' ],
	'active_callback' => [
		[
			'setting'  => 'custom_logo',
			'operator' => '!==',
			'value'    => '',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_branding_sitename_typography',
	'type'            => 'typography',
	'label'           => esc_html__( 'Site-Title Typography', 'q-theme' ),
	'description'     => esc_html__( 'Please select typography settings for your site-title.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header_branding',
	'default'         => [
		'font-family' => 'Noto Serif',
		'font-weight' => 700,
		'font-size'   => '32px',
		'color'       => '#000000',
	],
	'transport'       => 'auto',
	'output'          => [
		[
			'element' => '.q-tp-header_branding .site-title',
		],
		[
			'element'  => [ '.q-tp-header_branding .site-title a', '.q-tp-header_branding .site-title a:hover', '.q-tp-header_branding .site-title a:focus', '.q-tp-header_branding .site-title a:visited' ],
			'property' => 'color',
			'choice'   => 'color',
		],
		[
			'element'  => [ '.q-tp-header_branding .site-description' ],
			'property' => 'line-height',
			'choice'   => 'font-size',
		],
	],
	'active_callback' => 'display_header_text',
	'choices'         => [
		'fonts' => [
			'google' => [ 'popularity' ],
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_branding_tagline_typography',
	'type'            => 'typography',
	'label'           => esc_html__( 'Tagline Typography', 'q-theme' ),
	'description'     => esc_html__( 'Please select typography settings for your tagline.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header_branding',
	'default'         => [
		'font-family' => 'Noto Serif',
		'font-weight' => 'regular',
		'font-size'   => '16px',
		'color'       => '#000000',
	],
	'transport'       => 'auto',
	'output'          => [
		[
			'element' => '.q-tp-header_branding .site-description',
		],
	],
	'active_callback' => 'display_header_text',
	'choices'         => [
		'fonts' => [
			'google' => [ 'popularity' ],
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_branding_inline',
	'type'            => 'checkbox',
	'label'           => esc_html__( 'Inline Elements', 'q-theme' ),
	'description'     => esc_html__( 'Enable this option to show the branding elements inline instead of one below the other.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header_branding',
	'default'         => false,
	'transport'       => 'postMessage',
	'active_callback' => 'display_header_text',
	'partial_refresh' => [
		'q_theme_branding_inline_rendered' => [
			'selector'            => '.q-tp.q-tp-header',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'header' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_branding_inline_spacing',
	'type'            => 'slider',
	'label'           => esc_html__( 'Spacing between elements', 'q-theme' ),
	'description'     => esc_html__( 'This value is relevant to the site-title font-size.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header_branding',
	'default'         => .5,
	'transport'       => 'postMessage',
	'css_vars'        => [ '--q-branding-elements-padding', '$em' ],
	'choices'         => [
		'min'    => 0,
		'max'    => 5,
		'step'   => 0.01,
		'suffix' => 'em',
	],
	'active_callback' => 'display_header_text',
] );

q_theme_add_customizer_field( [
	'type'        => 'color',
	'settings'    => 'q_theme_grid_header_branding_background_color',
	'label'       => esc_html__( 'Background Color', 'q-theme' ),
	'description' => '',
	'section'     => 'q_theme_grid_part_details_header_branding',
	'default'     => '#ffffff',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-branding-bg',
	'choices'     => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'text',
	'settings'    => 'q_theme_grid_header_branding_padding',
	'label'       => esc_html__( 'Padding', 'q-theme' ),
	'description' => '',
	'section'     => 'q_theme_grid_part_details_header_branding',
	'default'     => '0.5em',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-branding-padding',
] );

q_theme_add_customizer_field( [
	'type'        => 'radio-buttonset',
	'settings'    => 'q_theme_grid_header_branding_horizontal_align',
	'label'       => esc_html__( 'Horizontal Alignment', 'q-theme' ),
	'description' => esc_html__( 'Choose the horizontal alignment for your branding', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_header_branding',
	'default'     => 'left',
	'transport'   => 'auto',
	'css_vars'    => '--q-branding-horizontal-align',
	'transport'   => 'postMessage',
	'choices'     => [
		'left'   => esc_html__( 'Left', 'q-theme' ),
		'center' => esc_html__( 'Center', 'q-theme' ),
		'right'  => esc_html__( 'Right', 'q-theme' ),
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'radio-buttonset',
	'settings'    => 'q_theme_grid_header_branding_vertical_align',
	'label'       => esc_html__( 'Vertical Alignment', 'q-theme' ),
	'description' => esc_html__( 'Choose the vertical alignment for your branding', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_header_branding',
	'default'     => 'center',
	'transport'   => 'auto',
	'css_vars'    => '--q-branding-vertical-align',
	'transport'   => 'postMessage',
	'choices'     => [
		'flex-start' => esc_html__( 'Top', 'q-theme' ),
		'center'     => esc_html__( 'Center', 'q-theme' ),
		'flex-end'   => esc_html__( 'Bottom', 'q-theme' ),
	],
] );
