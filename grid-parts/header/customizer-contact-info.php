<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Header;

q_theme_add_customizer_section( 'q_theme_grid_part_details_header_contact_info', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Header Contact Info', 'q-theme' ) ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/header/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'section'     => 'q_theme_grid',
	'priority'    => 20,
] );

q_theme_add_customizer_field( [
	'type'        => 'editor',
	'settings'    => 'q_theme_grid_part_details_header_contact_info',
	'label'       => esc_attr__( 'Contact Info Content', 'q-theme' ),
	'description' => esc_html__( 'Enter your contact info.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_header_contact_info',
	'default'     => __( 'Email: <a href="mailto:contact@example.com">contact@example.com</a>. Phone: +1-541-754-3010', 'q-theme' ),
	'transport'   => 'postMessage',
	'js_vars'     => [
		[
			'element'  => '.q-tp-header_contact_info.q-tp',
			'function' => 'html',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'color',
	'settings'  => 'q_theme_grid_part_details_header_contact_info_background_color',
	'label'     => esc_attr__( 'Background Color', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_header_contact_info',
	'default'   => '#ffffff',
	'transport' => 'postMessage',
	'css_vars'  => '--q-header-contact-bg',
	'choices'   => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'        => 'kirki-wcag-tc',
	'label'       => esc_html__( 'Text Color', 'q-theme' ),
	'description' => q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
	'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
	'settings'    => 'q_theme_grid_part_details_header_contact_info_text_color',
	'section'     => 'q_theme_grid_part_details_header_contact_info',
	'choices'     => [
		'setting' => 'q_theme_grid_part_details_header_contact_info_background_color',
	],
	'default'     => '#000000',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-header-contact-color',
] );

q_theme_add_customizer_field( [
	'type'        => 'slider',
	'settings'    => 'q_theme_grid_part_details_header_contact_info_font_size',
	'label'       => esc_attr__( 'Font Size', 'q-theme' ),
	'description' => esc_html__( 'Controls the font-size for your contact-info.', 'q-theme' ),
	'tooltip'     => q_theme()->customizer->get_text( 'related-font-size' ),
	'section'     => 'q_theme_grid_part_details_header_contact_info',
	'default'     => .85,
	'transport'   => 'postMessage',
	'css_vars'    => [ '--q-header-contact-font-size', '$em' ],
	'choices'     => [
		'min'    => .5,
		'max'    => 2,
		'step'   => .01,
		'suffix' => 'em',
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'dimension',
	'settings'  => 'q_theme_grid_part_details_header_contact_info_padding',
	'label'     => esc_attr__( 'Padding', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_header_contact_info',
	'default'   => '10px',
	'transport' => 'postMessage',
	'css_vars'  => '--q-header-contact-padding',
] );

q_theme_add_customizer_field( [
	'type'        => 'radio',
	'settings'    => 'q_theme_grid_part_details_header_contact_text_align',
	'label'       => esc_attr__( 'Text Align', 'q-theme' ),
	'description' => esc_html__( 'Select how the text will be aligned.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_header_contact_info',
	'default'     => 'flex-start',
	'transport'   => 'postMessage',
	'css_vars'    => '--q-header-contact-text-align',
	'choices'     => [
		'flex-start' => esc_html__( 'Left', 'q-theme' ),
		'center'     => esc_html__( 'Center', 'q-theme' ),
		'flex-end'   => esc_html__( 'Right', 'q-theme' ),
	],
] );
