<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Header;
use Q_Theme\Grid_Parts;

q_theme_add_customizer_section( 'q_theme_grid_part_details_header', [
	'title'       => esc_html__( 'Header', 'q-theme' ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/header/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 24,
] );

/**
 * Global toggle for mobile/desktop grid.
 */
q_theme_add_customizer_field( [
	'settings'          => 'q_theme_header_grid_toggle',
	'type'              => 'radio-buttonset',
	'label'             => esc_html__( 'Toggle between desktop & mobile grids.', 'q-theme' ),
	'section'           => 'q_theme_grid_part_details_header',
	'default'           => 'desktop',
	'priority'          => -10,
	'transport'         => 'postMessage',
	'choices'           => [
		'desktop' => esc_html__( 'Desktop', 'q-theme' ),
		'mobile'  => esc_html__( 'Mobile', 'q-theme' ),
	],
	'sanitize_callback' => function() {
		return 'desktop';
	},
] );

$header_grid_parts = Grid_Parts::get_instance()->get_parts();

// Remove parts that are not valid in this sub-grid.
$parts_to_remove = [ 'content', 'header', 'footer', 'nav-handheld', 'nested-grid-1', 'nested-grid-2', 'nested-grid-3', 'nested-grid-4' ];
foreach ( $header_grid_parts as $key => $part ) {
	if ( isset( $part['id'] ) && in_array( $part['id'], $parts_to_remove, true ) ) {
		unset( $header_grid_parts[ $key ] );
	}
}

/**
 * Remove header-textcolor control.
 * We have separate controls for title & subtitle so this one is not necessary.
 *
 * @since 1.0
 * @param object $wp_customize The WordPress Customizer instance.
 * @return void
 */
add_action( 'customize_register', function( $wp_customize ) {
	$wp_customize->remove_control( 'header_textcolor' );
} );

$header_grid_parts[] = [
	'label'    => esc_html__( 'Branding', 'q-theme' ),
	'color'    => '#DC3232',
	'priority' => 0,
	'hidden'   => false,
	'id'       => 'header_branding',
];

$header_grid_parts[] = [
	'label'    => esc_html__( 'Search', 'q-theme' ),
	'color'    => '#7b5e7b',
	'priority' => 200,
	'hidden'   => false,
	'id'       => 'header_search',
];

$header_grid_parts[] = [
	'label'    => esc_html__( 'Contact Information', 'q-theme' ),
	'color'    => '#093A3E',
	'priority' => 1000,
	'hidden'   => false,
	'id'       => 'header_contact_info',
];

$header_grid_parts[] = [
	'label'    => esc_html__( 'Social Media', 'q-theme' ),
	'color'    => '#0996c3',
	'priority' => 2000,
	'hidden'   => false,
	'id'       => 'social_media',
];

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_header_grid',
	'section'           => 'q_theme_grid_part_details_header',
	'type'              => 'q_theme_grid',
	'grid-part'         => 'header',
	'label'             => esc_html__( 'Grid Settings', 'q-theme' ),
	'description'       => sprintf(
		/* translators: Link attributes. */
		__( 'Edit settings for the grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
	),
	'default'           => Header::get_grid_defaults(),
	'choices'           => [
		'parts'     => $header_grid_parts,
		'duplicate' => 'q_theme_grid_header_mobile',
	],
	'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
	'transport'         => 'postMessage',
	'partial_refresh'   => [
		'q_theme_header_grid_part_renderer' => [
			'selector'            => '.q-tp.q-tp-header',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'header' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'        => 'q_theme_copy_header_grid_to_mobile',
	'type'            => 'custom',
	'label'           => esc_html__( 'Copy desktop grid setings to mobile grid', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => '<div style="margin-bottom:1em;"><button class="button-q-copy-grid-setting button button-primary button-large" data-from="q_theme_header_grid" data-to="q_theme_grid_header_mobile">' . esc_html__( 'Click here to copy settings from desktop grid', 'q-theme' ) . '</button></div>',
	'active_callback' => [
		[
			'setting'  => 'q_theme_header_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
] );

q_theme_add_customizer_field( [
	'settings'          => 'q_theme_grid_header_mobile',
	'section'           => 'q_theme_grid_part_details_header',
	'type'              => 'q_theme_grid',
	'grid-part'         => 'header',
	'label'             => esc_html__( 'Header Mobile Grid Settings', 'q-theme' ),
	'description'       => sprintf(
		/* translators: Link attributes. */
		__( 'Edit settings for the header grid. For more information and documentation on how the grid works, please read <a %s>this article</a>.', 'q-theme' ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'href="https://wplemon.com/documentation/q-theme/the-grid-control/" target="_blank"'
	),
	'default'           => Header::get_grid_mobile_defaults(),
	'sanitize_callback' => [ q_theme()->customizer, 'sanitize_q_theme_grid' ],
	'choices'           => [
		'parts'              => $header_grid_parts,
		'duplicate'          => 'q_theme_header_grid',
		'disablePartButtons' => [ 'edit' ],
	],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
	],
	'transport'         => 'postMessage',
	'partial_refresh'   => [
		'q_theme_header_mobile_grid_part_renderer' => [
			'selector'            => '.q-tp.q-tp-header',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'header' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_header_max_width',
	'label'           => esc_attr__( 'Max-Width', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'grid-part-max-width' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => '',
	'css_vars'        => '--q-header-max-width',
	'transport'       => 'postMessage',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'dimension',
	'settings'        => 'q_theme_grid_header_grid_gap',
	'label'           => esc_attr__( 'Grid Gap', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'grid-gap-description' ),
	'tooltip'         => esc_html__( 'If you have a background-color or background-image defined for your header, then these will be visible through these gaps which creates a unique appearance since each grid-part looks separate.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => '0',
	'css_vars'        => '--q-header-grid-gap',
	'transport'       => 'postMessage',
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'color',
	'settings'        => 'q_theme_grid_part_details_header_background_color',
	'label'           => esc_attr__( 'Background', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => '#ffffff',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-header-bg',
	'choices'         => [
		'alpha' => true,
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
	'priority'        => 70,
] );

q_theme_add_customizer_field( [
	'type'            => 'radio',
	'settings'        => 'q_theme_grid_header_box_shadow',
	'label'           => esc_html__( 'Drop Shadow Intensity', 'q-theme' ),
	'description'     => esc_html__( 'Set to "None" if you want to disable the shadow for this grid-part, or increase the intensity for a more dramatic effect.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
	'transport'       => 'postMessage',
	'css_vars'        => '--q-header-box-shadow',
	'priority'        => 200,
	'choices'         => [
		'none' => esc_html__( 'None', 'q-theme' ),
		'0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)' => esc_html__( 'Extra Light', 'q-theme' ),
		'0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)' => esc_html__( 'Light', 'q-theme' ),
		'0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)' => esc_html__( 'Medium', 'q-theme' ),
		'0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)' => esc_html__( 'Heavy', 'q-theme' ),
		'0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)' => esc_html__( 'Extra Heavy', 'q-theme' ),
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
	'priority'        => 90,
] );

q_theme_add_customizer_field( [
	'type'            => 'toggle',
	'settings'        => 'q_theme_header_sticky',
	'label'           => esc_attr__( 'Sticky', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'sticky-description' ),
	'tooltip'         => q_theme()->customizer->get_text( 'sticky-tooltip' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => false,
	'transport'       => 'refresh',
	'priority'        => 300,
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'desktop',
		],
	],
	'priority'        => 91,
] );

q_theme_add_customizer_field( [
	'type'            => 'toggle',
	'settings'        => 'q_theme_header_sticky_mobile',
	'label'           => esc_attr__( 'Sticky (Mobile)', 'q-theme' ),
	'description'     => q_theme()->customizer->get_text( 'sticky-description' ),
	'tooltip'         => q_theme()->customizer->get_text( 'sticky-tooltip' ),
	'section'         => 'q_theme_grid_part_details_header',
	'default'         => false,
	'transport'       => 'refresh',
	'priority'        => 300,
	'active_callback' => [
		[
			'setting'  => 'q_theme_global_grid_toggle',
			'operator' => '===',
			'value'    => 'mobile',
		],
		[
			'setting'  => 'q_theme_header_sticky',
			'operator' => '===',
			'value'    => true,
		],
	],
	'priority'        => 91,
] );
