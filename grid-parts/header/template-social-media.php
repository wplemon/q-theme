<?php
/**
 * Template part for the Header Social-Media.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Grid_Part\Header;
use Q_Theme\Style;

$setting = get_theme_mod( 'q_theme_grid_part_details_social_icons', Header::social_icons_default_value() );
if ( ! function_exists( 'q_theme_social_icons_svg' ) ) {

	// Include Social Icons Definitions.
	require_once get_template_directory() . '/inc/social-icons.php';
}
$icons = q_theme_social_icons_svg();

// Init Style class.
$style = new Style();

// Add CSS-vars.
$style->add_vars( [
	'--q-header-social-icons-bg'         => get_theme_mod( 'q_theme_grid_part_details_social_icons_background_color', '#ffffff' ),
	'--q-header-social-icons-text-align' => get_theme_mod( 'q_theme_grid_part_details_social_icons_icons_text_align', 'right' ),
	'--q-header-social-icons-size'       => get_theme_mod( 'q_theme_grid_part_details_social_icons_size', .85 ) . 'em',
	'--q-header-social-icons-padding'    => get_theme_mod( 'q_theme_grid_part_details_social_icons_padding', .5 ) . 'em',
	'--q-header-social-icons-color'      => get_theme_mod( 'q_theme_grid_part_details_social_icons_icons_color', '#000000' ),
] );

// Add stylesheet.
$style->add_file( get_theme_file_path( 'grid-parts/header/styles/social-icons.min.css' ) );
?>

<div class="q-tp q-tp-social_media">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-header-social-media' );
	?>
	<?php foreach ( $setting as $icon ) : ?>
		<?php if ( ! empty( $icon['url'] ) && ! empty( $icon['icon'] ) && isset( $icons[ $icon['icon'] ] ) ) : ?>
			<?php $url = ( 'mail' === $icon['icon'] ) ? 'mailto:' . antispambot( $icon['url'] ) : esc_url_raw( $icon['url'] ); ?>
			<a href="<?php echo $url; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( $icon['icon'] ); ?>">
				<?php echo $icons[ $icon['icon'] ]; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			</a>
		<?php endif; ?>
	<?php endforeach; ?>
</div>
