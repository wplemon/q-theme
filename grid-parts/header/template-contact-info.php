<?php
/**
 * Template part for the Header Contact Info.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Style;

$style = new Style();
$style->add_file( get_theme_file_path( 'grid-parts/header/styles/contact-info.min.css' ) );
$style->add_vars( [
	'--q-header-contact-bg'         => get_theme_mod( 'q_theme_grid_part_details_header_contact_info_background_color', '#ffffff' ),
	'--q-header-contact-font-size'  => get_theme_mod( 'q_theme_grid_part_details_header_contact_info_font_size', .85 ) . 'em',
	'--q-header-contact-text-align' => get_theme_mod( 'q_theme_grid_part_details_header_contact_text_align', 'flex-start' ),
	'--q-header-contact-padding'    => get_theme_mod( 'q_theme_grid_part_details_header_contact_info_padding', '10px' ),
	'--q-header-contact-color'      => get_theme_mod( 'q_theme_grid_part_details_header_contact_info_text_color', '#000000' ),
] );

/**
 * Print styles.
 */
$style->the_css( 'q-inline-css-header-contact-info' );
?>
<div class="q-tp q-tp-header_contact_info">
	<?php
	/**
	 * Print the text entered by the user.
	 */
	echo wp_kses_post( get_theme_mod( 'q_theme_grid_part_details_header_contact_info', __( 'Email: <a href="mailto:contact@example.com">contact@example.com</a>. Phone: +1-541-754-3010', 'q-theme' ) ) );
	?>
</div>
