<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Header;

q_theme_add_customizer_section( 'q_theme_grid_part_details_header_search', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Header Search', 'q-theme' ) ),
	'section'     => 'q_theme_grid',
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/header/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 20,
] );

q_theme_add_customizer_field( [
	'type'      => 'dimensions',
	'settings'  => 'q_theme_grid_part_details_header_search_padding',
	'label'     => esc_attr__( 'Padding', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_header_search',
	'default'   => [
		'left'  => '1em',
		'right' => '1em',
	],
	'transport' => 'postMessage',
	'css_vars'  => [
		[ '--q-header-search-padding-left', '$', 'left' ],
		[ '--q-header-search-padding-right', '$', 'right' ],
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'slider',
	'settings'  => 'q_theme_grid_part_details_header_search_font_size',
	'label'     => esc_attr__( 'Font Size', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_header_search',
	'default'   => 1,
	'transport' => 'postMessage',
	'css_vars'  => [ '--q-header-search-font-size', '$em' ],
	'choices'   => [
		'min'    => .7,
		'max'    => 2,
		'step'   => .01,
		'suffix' => 'em',
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'color',
	'settings'  => 'q_theme_grid_part_details_header_bg_color',
	'label'     => esc_attr__( 'Background Color', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_header_search',
	'default'   => '#ffffff',
	'transport' => 'postMessage',
	'css_vars'  => '--q-header-search-bg',
	'choices'   => [
		'alpha' => true,
	],
] );

q_theme_add_customizer_field( [
	'type'      => 'color',
	'settings'  => 'q_theme_grid_part_details_header_search_color',
	'label'     => esc_attr__( 'Text Color', 'q-theme' ),
	'section'   => 'q_theme_grid_part_details_header_search',
	'default'   => '#000000',
	'transport' => 'postMessage',
	'css_vars'  => '--q-header-search-color',
] );
