<?php
/**
 * Template part for the Header Search
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Grid_Part\Header;
use Q_Theme\Style;

$style = new Style();
$style->add_string( '.q-header-search{background-color:var(--q-header-search-bg);padding-left:var(--q-header-search-padding-left);padding-right: var(--q-header-search-padding-right);}' );
$style->add_string( '.q-header-search form.search-form{font-size:var(--q-header-search-font-size);color:var(--q-header-search-color);}' );
$style->add_string( '.q-header-search form.search-form>label>input.search-field{border-bottom:none;}' );
$padding = get_theme_mod( 'q_theme_grid_part_details_header_search_padding', [
	'left'  => '1em',
	'right' => '1em',
] );
$style->add_vars( [
	'--q-header-search-bg'            => get_theme_mod( 'q_theme_grid_part_details_header_bg_color', '#ffffff' ),
	'--q-header-search-padding-left'  => $padding['left'],
	'--q-header-search-padding-right' => $padding['right'],
	'--q-header-search-font-size'     => get_theme_mod( 'q_theme_grid_part_details_header_search_font_size', 1 ) . 'em',
	'--q-header-search-color'         => get_theme_mod( 'q_theme_grid_part_details_header_search_color', '#000000' ),
] );
?>

<div class="q-tp q-tp-header_search">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-header-search' );
	?>

	<div class="q-header-search inner" style="display:flex;align-items:center;width:100%;">
		<?php get_search_form( true ); ?>
	</div>
</div>
