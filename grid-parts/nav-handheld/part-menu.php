<?php
/**
 * Template part for the handheld navigation.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\Style;
use Q_Theme\Grid_Part\Navigation;

$style = new Style();
$style->add_string( Navigation::get_global_styles() );
$style->add_file( get_theme_file_path( 'grid-parts/nav-handheld/styles/navigation.min.css' ) );
$style->add_vars( [
	'--q-text-color'  => get_theme_mod( 'q_theme_text_color', '#000000' ),
	'--q-links-color' => get_theme_mod( 'q_theme_links_color', '#0f5e97' ),
	'--q-content-bg'  => get_theme_mod( 'q_theme_grid_content_background_color', '#ffffff' ),
] );
$style->the_css( 'q-inline-css-nav-handheld-menu' );

$label_class = get_theme_mod( 'q_theme_grid_nav-handheld_hide_labels', false ) ? 'screen-reader-text' : 'label';
?>
<nav id="q-handheld-nav" class="q-nav-vertical">
	<?php
	/**
	 * Prints the button.
	 */
	echo q_theme_toggle_button( [ // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		'expanded_state_id' => 'qHandheldNavMenu',
		'expanded'          => 'false',
		// 'screen_reader_label_collapse' => __( 'Collapse Navigation', 'q-theme' ),
		// 'screen_reader_label_expand'   => __( 'Expand Navigation', 'q-theme' ),
		// 'screen_reader_label_toggle'   => __( 'Toggle Navigation', 'q-theme' ),
		'label'             => '<svg class="q-inline-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M24 6h-24v-4h24v4zm0 4h-24v4h24v-4zm0 8h-24v4h24v-4z"/></svg><span class="' . $label_class . '">' . esc_html__( 'Menu', 'q-theme' ) . '</span>',
		'classes'           => [ 'q-nav-handheld-btn' ],
	] );
	?>
	<div id="q-handheld-menu-wrapper" class="q-hanheld-nav-popup-wrapper">
		<?php
		wp_nav_menu( [
			'theme_location' => 'menu-handheld',
			'menu_id'        => 'nav-handheld',
			'item_spacing'   => 'discard',
			'container'      => false,
		] );
		?>
	</div>
</nav>
