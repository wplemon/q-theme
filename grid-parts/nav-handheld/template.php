<?php
/**
 * Template part for the handheld navigation.
 *
 * @package Q Theme
 * @since 1.0
 */

use Q_Theme\AMP;
use Q_Theme\Style;

// Add styles.
$style = new Style();
$style->add_file( get_theme_file_path( 'grid-parts/nav-handheld/styles/styles.min.css' ) );
$style->add_string( '@media only screen and (min-width:' . get_theme_mod( 'q_mobile_breakpoint', '800px' ) . '){.q-tp.q-tp-nav-handheld{display: none;}body{padding-bottom:0;}}' );
?>
<div class="q-tp q-tp-nav-handheld">
	<?php
	if ( get_theme_mod( 'q_theme_grid_nav-handheld_enable', true ) ) {

		// Get the array of parts we want to show.
		$default = class_exists( 'WooCommerce' ) ? [ 'menu', 'home', 'search', 'woo-cart' ] : [ 'menu', 'home', 'search' ];
		$parts   = get_theme_mod( 'q_theme_grid_nav-handheld_parts', $default );

		// Check that we want to show something.
		if ( ! empty( $parts ) ) {

			// Print styles.
			$style->the_css( 'q-inline-css-nav-handheld' );

			// Print the parts.
			foreach ( $parts as $part ) {
				get_template_part( 'grid-parts/nav-handheld/part', $part );
			}
		}
	}
	?>
</div>
