<?php
/**
 * Customizer options for handheld nav.
 *
 * @package Q Theme
 */

q_theme_add_customizer_section( 'q_theme_grid_part_details_nav-handheld', [
	'title'       => esc_html__( 'Mobile Navigation', 'q-theme' ),
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/mobile-navigation/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 26,
] );

// The parts available for handheld-nav.
$parts = [
	'menu'        => esc_html__( 'Menu', 'q-theme' ),
	'home'        => esc_html__( 'Home', 'q-theme' ),
	'widget-area' => esc_html__( 'Widget Area', 'q-theme' ),
	'search'      => esc_html__( 'Search', 'q-theme' ),
];

// If WooCommerce is installed, add another item for the Cart.
if ( class_exists( 'WooCommerce' ) ) {
	$parts['woo-cart'] = esc_html__( 'Cart', 'q-theme' );
}

q_theme_add_customizer_field( [
	'type'            => 'checkbox',
	'settings'        => 'q_theme_grid_nav-handheld_enable',
	'label'           => esc_html__( 'Enable Mobile Navigation', 'q-theme' ),
	'description'     => esc_html__( 'Enables the mobile navigation for devices smaller than the breakpoint defined in your grid settings.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_nav-handheld',
	'default'         => true,
	'transport'       => 'postMessage',
	'partial_refresh' => [
		'q_theme_grid_nav-handheld_enable_template' => [
			'selector'            => '.q-tp-nav-handheld',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'nav-handheld' );
			},
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'sortable',
	'settings'        => 'q_theme_grid_nav-handheld_parts',
	'label'           => esc_html__( 'Mobile Navigation active parts & order', 'q-theme' ),
	'description'     => esc_html__( 'Enable and disable parts of the mobile navigation, and reorder them at will.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_nav-handheld',
	'default'         => class_exists( 'WooCommerce' ) ? [ 'menu', 'home', 'search', 'woo-cart' ] : [ 'menu', 'home', 'search' ],
	'choices'         => $parts,
	'transport'       => 'postMessage',
	'partial_refresh' => [
		'grid_part_handheld_parts' => [
			'selector'            => '.q-tp-nav-handheld',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'nav-handheld' );
			},
		],
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_grid_nav-handheld_enable',
			'operator' => '===',
			'value'    => true,
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'text',
	'settings'        => 'q_theme_grid_nav-handheld_widget_area_label',
	'label'           => esc_attr__( 'Widget Area Button Label', 'q-theme' ),
	'description'     => esc_html__( 'Please add a label for the widget area. This label is used by screen-readers so that people with a visual impairment have an auditory hint about what the button does.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_nav-handheld',
	'default'         => esc_html__( 'Settings', 'q-theme' ),
	'transport'       => 'postMessage',
	'partial_refresh' => [
		'q_theme_grid_nav-handheld_widget_area_label_template' => [
			'selector'            => '.q-tp-nav-handheld',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'nav-handheld' );
			},
		],
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_grid_nav-handheld_enable',
			'operator' => '===',
			'value'    => true,
		],
		[
			'setting'  => 'q_theme_grid_nav-handheld_parts',
			'operator' => 'contains',
			'value'    => 'widget-area',
		],
	],
] );

q_theme_add_customizer_field( [
	'type'              => 'textarea',
	'settings'          => 'q_theme_grid_nav-handheld_widget_area_icon',
	'label'             => esc_attr__( 'Widget Area Button SVG Icon', 'q-theme' ),
	'description'       => __( 'Paste SVG code for the icon you want to use. You can find a great collection of icons on the <a href="https://iconmonstr.com/" target="_blank" rel="noopener noreferrer nofollow">iconmonstr website</a> or add your custom icon.', 'q-theme' ),
	'section'           => 'q_theme_grid_part_details_nav-handheld',
	'default'           => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 13.616v-3.232c-1.651-.587-2.694-.752-3.219-2.019v-.001c-.527-1.271.1-2.134.847-3.707l-2.285-2.285c-1.561.742-2.433 1.375-3.707.847h-.001c-1.269-.526-1.435-1.576-2.019-3.219h-3.232c-.582 1.635-.749 2.692-2.019 3.219h-.001c-1.271.528-2.132-.098-3.707-.847l-2.285 2.285c.745 1.568 1.375 2.434.847 3.707-.527 1.271-1.584 1.438-3.219 2.02v3.232c1.632.58 2.692.749 3.219 2.019.53 1.282-.114 2.166-.847 3.707l2.285 2.286c1.562-.743 2.434-1.375 3.707-.847h.001c1.27.526 1.436 1.579 2.019 3.219h3.232c.582-1.636.75-2.69 2.027-3.222h.001c1.262-.524 2.12.101 3.698.851l2.285-2.286c-.744-1.563-1.375-2.433-.848-3.706.527-1.271 1.588-1.44 3.221-2.021zm-12 2.384c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"/></svg>',
	'transport'         => 'postMessage',
	'sanitize_callback' => function( $value ) {
		return $value;
	},
	'partial_refresh'   => [
		'grid_part_handheld_widget_area_icon' => [
			'selector'            => '.q-tp-nav-handheld',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'nav-handheld' );
			},
		],
	],
	'active_callback'   => [
		[
			'setting'  => 'q_theme_grid_nav-handheld_parts',
			'operator' => 'contains',
			'value'    => 'widget-area',
		],
		[
			'setting'  => 'q_theme_grid_nav-handheld_enable',
			'operator' => '===',
			'value'    => true,
		],
	],
] );

q_theme_add_customizer_field( [
	'type'            => 'checkbox',
	'settings'        => 'q_theme_grid_nav-handheld_hide_labels',
	'label'           => esc_attr__( 'Hide Labels', 'q-theme' ),
	'description'     => __( 'Enable this option if you want to hide the button labels. If labels are hidden, they only become available to screen-readers.', 'q-theme' ),
	'section'         => 'q_theme_grid_part_details_nav-handheld',
	'default'         => false,
	'transport'       => 'postMessage',
	'partial_refresh' => [
		'q_theme_grid_nav_handheld_hide_labels_rendered' => [
			'selector'            => '.q-tp-nav-handheld',
			'container_inclusive' => true,
			'render_callback'     => function() {
				do_action( 'q_theme_the_grid_part', 'nav-handheld' );
			},
		],
	],
	'active_callback' => [
		[
			'setting'  => 'q_theme_grid_nav-handheld_enable',
			'operator' => '===',
			'value'    => true,
		],
	],
] );
