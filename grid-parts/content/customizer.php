<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

q_theme_add_customizer_section( 'q_theme_grid_part_details_content', [
	/* translators: The grid-part label. */
	'title'       => sprintf( esc_attr__( '%s Options', 'q-theme' ), esc_html__( 'Content', 'q-theme' ) ),
	'section'     => 'q_theme_grid',
	'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/content/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
	'priority'    => 90,
] );

q_theme_add_customizer_field( [
	'type'        => 'dimension',
	'settings'    => 'q_theme_grid_content_max_width',
	'label'       => esc_attr__( 'Max-Width', 'q-theme' ),
	'description' => q_theme()->customizer->get_text( 'grid-part-max-width' ),
	'section'     => 'q_theme_grid_part_details_content',
	'default'     => '45em',
	'css_vars'    => '--q-content-max-width',
	'transport'   => 'postMessage',
	'priority'    => 10,
] );

q_theme_add_customizer_field( [
	'type'        => 'dimensions',
	'settings'    => 'q_theme_grid_content_padding',
	'label'       => esc_attr__( 'Container Padding', 'q-theme' ),
	'description' => esc_html__( 'Please enter values for the content area\'s padding.', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_content',
	'default'     => [
		'top'    => '0px',
		'bottom' => '0px',
		'left'   => '20px',
		'right'  => '20px',
	],
	'css_vars'    => [
		[ '--q-content-padding-top', '$', 'top' ],
		[ '--q-content-padding-bottom', '$', 'bottom' ],
		[ '--q-content-padding-left', '$', 'left' ],
		[ '--q-content-padding-right', '$', 'right' ],
	],
	'transport'   => 'postMessage',
	'priority'    => 15,
] );

q_theme_add_customizer_field( [
	'type'        => 'color',
	'settings'    => 'q_theme_grid_content_background_color',
	'label'       => esc_attr__( 'Background Color', 'q-theme' ),
	'description' => esc_html__( 'Background Color for the content area. Always prefer light backgrounds with dark text for increased accessibility', 'q-theme' ),
	'section'     => 'q_theme_grid_part_details_content',
	'default'     => '#ffffff',
	'css_vars'    => '--q-content-bg',
	'transport'   => 'postMessage',
	'priority'    => 30,
	'choices'     => [
		'alpha' => true,
	],
] );

/**
 * Focus on title_tagline section.
 */
q_theme_add_customizer_field( [
	'settings' => 'q_theme_logo_focus_on_typography_section',
	'type'     => 'custom',
	'label'    => 'Looking for the text-color and typography options?',
	'section'  => 'q_theme_grid_part_details_content',
	'priority' => 32,
	'default'  => '<div style="margin-bottom:1em;"><button class="button-q-focus global-focus button button-primary button-large" data-context="section" data-focus="q_theme_typography">' . esc_html__( 'Click here to edit typography options', 'q-theme' ) . '</button></div>',
] );
