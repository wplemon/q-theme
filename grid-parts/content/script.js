var qComputeEm = function() {
	var root = document.querySelectorAll( ':root' )[0],
		style = getComputedStyle( document.body ),
		em = style.getPropertyValue( 'font-size' ),
		contentWidth = style.getPropertyValue( '--q-content-max-width' ),
		maxWidthEm = -1 === contentWidth.indexOf( 'rem' ) && -1 !== contentWidth.indexOf( 'em' ),
		width = maxWidthEm ? parseInt( parseFloat( contentWidth, 10 ) * parseFloat( em ), 10 ) + 'px' : contentWidth;

	root.style.setProperty( '--qem', em );
	root.style.setProperty( '--q-content-max-width-calculated', width );
};

window.addEventListener( 'resize', function() {
	qComputeEm();
}, window.qSupportsPassive ? {
	passive: true
} : false );
qComputeEm();
