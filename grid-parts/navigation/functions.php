<?php
/**
 * Init Navigation.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Navigation;

new Navigation();
