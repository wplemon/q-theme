<?php
/**
 * Customizer options.
 *
 * @package Q Theme
 */

use Q_Theme\Grid_Part\Navigation;

/**
 * Register the menus.
 *
 * @since 1.0
 * @return void
 */
function q_theme_add_nav_parts() {
	$number = Navigation::get_number_of_nav_menus();

	for ( $i = 1; $i <= $number; $i++ ) {
		q_theme_nav_customizer_options( $i );
	}
}
add_action( 'after_setup_theme', 'q_theme_add_nav_parts' );

/**
 * This function creates all options for a navigation.
 * We use a parameter since we'll allow multiple navigations.
 *
 * @since 1.0
 * @param int $id The number of this navigation.
 * @return void
 */
function q_theme_nav_customizer_options( $id ) {

	/**
	 * Add Customizer Sections.
	 */
	q_theme_add_customizer_section( "q_theme_grid_part_details_nav_$id", [
		'title'       => sprintf(
			/* translators: The grid-part label. */
			esc_attr__( '%s Options', 'q-theme' ),
			/* translators: The navigation number. */
			sprintf( esc_html__( 'Navigation %d', 'q-theme' ), absint( $id ) )
		),
		'description' => '<a href="https://wplemon.com/documentation/q-theme/grid-parts/navigation/" target="_blank" rel="noopener noreferrer nofollow">' . esc_html__( 'Learn more about these settings', 'q-theme' ),
		'section'     => 'q_theme_grid',
	] );

	/**
	 * Focus on menu_locations section.
	 */
	q_theme_add_customizer_field( [
		'settings' => "q_theme_logo_focus_on_menu_locations_$id",
		'type'     => 'custom',
		'label'    => esc_html__( 'Looking for your menu items?', 'q-theme' ),
		'section'  => "q_theme_grid_part_details_nav_$id",
		'default'  => '<div style="margin-bottom:1em;"><button class="button-q-focus global-focus button button-primary button-large" data-context="section" data-focus="menu_locations">' . esc_html__( 'Click here to edit your menus', 'q-theme' ) . '</button></div>',
	] );

	q_theme_add_customizer_field( [
		'type'     => 'custom',
		'settings' => "q_theme_grid_nav_{$id}_responsive_title_helper",
		'section'  => "q_theme_grid_part_details_nav_$id",
		'default'  => '<h3 class="q-customizer-section-title">' . esc_html__( 'Responsive Options', 'q-theme' ) . '</h3>',
	] );

	q_theme_add_customizer_field( [
		'type'            => 'radio',
		'settings'        => "q_theme_grid_nav_{$id}_collapse_to_icon",
		'label'           => esc_attr__( 'Mobile Menu Mode', 'q-theme' ),
		'description'     => esc_html__( 'Select when this menu should collapse to an icon.', 'q-theme' ),
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => 'mobile',
		'transport'       => 'postMessage',
		'choices'         => [
			'never'  => esc_html__( 'Never', 'q-theme' ),
			'mobile' => esc_html__( 'Only on Mobile', 'q-theme' ),
			'always' => esc_html__( 'Always', 'q-theme' ),
		],
		'partial_refresh' => [
			"q_theme_grid_nav_{$id}_collapse_to_icon_template" => [
				'selector'            => ".q-tp-nav_{$id}",
				'container_inclusive' => true,
				'render_callback'     => function() use ( $id ) {
					/**
					 * We use include( get_theme_file_path() ) here
					 * because we need to pass the $id var to the template.
					 */
					include get_theme_file_path( 'grid-parts/navigation/template.php' );
				},
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'     => 'custom',
		'settings' => "q_theme_grid_nav_{$id}_styles_title_helper",
		'section'  => "q_theme_grid_part_details_nav_$id",
		'default'  => '<h3 class="q-customizer-section-title">' . esc_html__( 'Styles', 'q-theme' ) . '</h3>',
	] );

	q_theme_add_customizer_field( [
		'type'        => 'dimension',
		'settings'    => "q_theme_grid_nav_{$id}_padding",
		'label'       => esc_attr__( 'Padding', 'q-theme' ),
		'description' => esc_html__( 'Inner padding for this grid-part. Use any valid CSS value.', 'q-theme' ),
		'tooltip'     => q_theme()->customizer->get_text( 'padding' ),
		'section'     => "q_theme_grid_part_details_nav_$id",
		'default'     => '1em',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nav-$id-padding",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'slider',
		'settings'    => "q_theme_grid_nav_{$id}_font_size",
		'label'       => esc_html__( 'Menu Font Size', 'q-theme' ),
		'description' => esc_html__( 'The font-size of the menu in relation to the body font-size.', 'q-theme' ),
		'tooltip'     => q_theme()->customizer->get_text( 'related-font-size' ),
		'section'     => "q_theme_grid_part_details_nav_$id",
		'default'     => 1,
		'transport'   => 'postMessage',
		'css_vars'    => [ "--q-nav-$id-font-size", '$em' ],
		'choices'     => [
			'min'    => 0.5,
			'max'    => 2.5,
			'step'   => 0.01,
			'suffix' => 'em',
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'slider',
		'settings'    => "q_theme_grid_nav_{$id}_items_padding",
		'label'       => esc_html__( 'Items Padding', 'q-theme' ),
		'description' => esc_html__( 'The inner padding for menu items.', 'q-theme' ),
		'tooltip'     => q_theme()->customizer->get_text( 'related-font-size' ),
		'section'     => "q_theme_grid_part_details_nav_$id",
		'default'     => 1,
		'transport'   => 'postMessage',
		'css_vars'    => [ "--q-nav-$id-items-padding", '$em' ],
		'choices'     => [
			'min'    => 0.2,
			'max'    => 3,
			'step'   => 0.01,
			'suffix' => 'em',
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'color',
		'label'       => esc_html__( 'Background Color', 'q-theme' ),
		'description' => esc_html__( 'Controls the overall background color for this grid-part.', 'q-theme' ),
		'settings'    => "q_theme_grid_nav_{$id}_bg_color",
		'section'     => "q_theme_grid_part_details_nav_$id",
		'default'     => '#ffffff',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nav-$id-bg",
		'choices'     => [
			'alpha' => true,
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'kirki-wcag-tc',
		'label'       => esc_html__( 'Items Color', 'q-theme' ),
		'description' => esc_html__( 'Controls the color of menu items.', 'q-theme' ) . '<br>' . q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
		'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
		'settings'    => "q_theme_grid_nav_{$id}_items_color",
		'section'     => "q_theme_grid_part_details_nav_$id",
		'choices'     => [
			'setting' => "q_theme_grid_nav_{$id}_bg_color",
		],
		'default'     => '#000000',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nav-$id-color",
	] );

	q_theme_add_customizer_field( [
		'type'        => 'color',
		'label'       => esc_html__( 'Accent Background Color', 'q-theme' ),
		'description' => esc_html__( 'Controls the background-color for menu items on hover, as well as for the currently active menu-item. Applies to both parent and child (dropdown) menu items.', 'q-theme' ),
		'settings'    => "q_theme_grid_nav_{$id}_accent_bg_color",
		'section'     => "q_theme_grid_part_details_nav_$id",
		'default'     => '#0f5e97',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nav-$id-accent-bg",
		'choices'     => [
			'alpha' => true,
		],
	] );

	q_theme_add_customizer_field( [
		'type'        => 'kirki-wcag-tc',
		'label'       => esc_html__( 'Accent Items Color', 'q-theme' ),
		'description' => esc_html__( 'Controls the text color for menu items on hover, as well as for the currently active menu-item. Applies to both parent and child (dropdown) menu items.', 'q-theme' ) . '<br>' . q_theme()->customizer->get_text( 'a11y-textcolor-description' ),
		'tooltip'     => q_theme()->customizer->get_text( 'a11y-textcolor-tooltip' ),
		'settings'    => "q_theme_grid_nav_{$id}_accent_color",
		'section'     => "q_theme_grid_part_details_nav_$id",
		'choices'     => [
			'setting' => "q_theme_grid_nav_{$id}_accent_bg_color",
		],
		'default'     => '#ffffff',
		'transport'   => 'postMessage',
		'css_vars'    => "--q-nav-$id-accent-color",
	] );

	q_theme_add_customizer_field( [
		'type'            => 'custom',
		'settings'        => "q_theme_grid_nav_{$id}_desktop_menu_title_helper",
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => '<h3 class="q-customizer-section-title">' . esc_html__( 'Desktop Menu Properties', 'q-theme' ) . '</h3>',
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'mobile',
				'operator' => '===',
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'            => 'checkbox',
		'settings'        => "q_theme_grid_nav_{$id}_vertical",
		'label'           => esc_attr__( 'Enable Vertical Menu Mode', 'q-theme' ),
		'description'     => esc_html__( 'If your layout is column-based and you want a vertical side-navigation enable this option.', 'q-theme' ),
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => false,
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'always',
				'operator' => '!==',
			],
		],
		'transport'       => 'postMessage',
		'partial_refresh' => [
			"q_theme_grid_nav_{$id}_vertical_template" => [
				'selector'            => ".q-tp-nav_{$id}",
				'container_inclusive' => true,
				'render_callback'     => function() use ( $id ) {
					/**
					 * We use include( get_theme_file_path() ) here 
					 * because we need to pass the $id var to the template.
					 */
					include get_theme_file_path( 'grid-parts/navigation/template.php' );
				},
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'            => 'radio-image',
		'settings'        => "q_theme_grid_nav_{$id}_justify_content",
		'label'           => esc_attr__( 'Justify Items', 'q-theme' ),
		'description'     => esc_html__( 'Choose how menu items will be spread horizontally inside the menu container.', 'q-theme' ),
		'tooltip'         => esc_html__( 'This helps distribute extra free space left over when all the items on a line have reached their maximum size. It also exerts some control over the alignment of items when they overflow the line.', 'q-theme' ),
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => 'center',
		'transport'       => 'postMessage',
		'css_vars'        => "--q-nav-$id-justify",
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_vertical",
				'operator' => '!==',
				'value'    => true,
			],
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'always',
				'operator' => '!==',
			],
		],
		'choices'         => [
			'flex-start'    => [
				'src' => get_template_directory_uri() . '/assets/images/justify-content/start.png',
				'alt' => esc_html__( 'Start', 'q-theme' ),
			],
			'flex-end'      => [
				'src' => get_template_directory_uri() . '/assets/images/justify-content/end.png',
				'alt' => esc_html__( 'End', 'q-theme' ),
			],
			'center'        => [
				'src' => get_template_directory_uri() . '/assets/images/justify-content/center.png',
				'alt' => esc_html__( 'Center', 'q-theme' ),
			],
			'space-between' => [
				'src' => get_template_directory_uri() . '/assets/images/justify-content/space-between.png',
				'alt' => esc_html__( 'Space Between', 'q-theme' ),
			],
			'space-around'  => [
				'src' => get_template_directory_uri() . '/assets/images/justify-content/space-around.png',
				'alt' => esc_html__( 'Space Around', 'q-theme' ),
			],
			'space-evenly'  => [
				'src' => get_template_directory_uri() . '/assets/images/justify-content/space-evenly.png',
				'alt' => esc_html__( 'Space Evenly', 'q-theme' ),
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'            => 'custom',
		'settings'        => "q_theme_grid_nav_{$id}_mobile_menu_title_helper",
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => '<h3 class="q-customizer-section-title">' . esc_html__( 'Mobile Menu Properties', 'q-theme' ) . '</h3>',
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'mobile',
				'operator' => '===',
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'            => 'radio-buttonset',
		'settings'        => "q_theme_grid_nav_{$id}_expand_icon",
		'label'           => esc_attr__( 'Expand Icon', 'q-theme' ),
		'description'     => esc_html__( 'Select the icon that should be used to expand the navigation.', 'q-theme' ),
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => 'plus-5',
		'transport'       => 'refresh',
		'choices'         => Navigation::get_expand_svgs(),
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'never',
				'operator' => '!==',
			],
		],
		'transport'       => 'postMessage',
		'partial_refresh' => [
			"q_theme_grid_nav_{$id}_expand_icon_template" => [
				'selector'            => ".q-tp-nav_{$id}",
				'container_inclusive' => true,
				'render_callback'     => function() use ( $id ) {
					/**
					 * We use include( get_theme_file_path() ) here 
					 * because we need to pass the $id var to the template.
					 */
					include get_theme_file_path( 'grid-parts/navigation/template.php' );
				},
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'            => 'radio-buttonset',
		'settings'        => "q_theme_grid_nav_{$id}_expand_icon_position",
		'label'           => esc_attr__( 'Expand Icon Position', 'q-theme' ),
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => 'center-right',
		'transport'       => 'refresh',
		'choices'         => [
			'top-left'      => esc_html__( 'Top Left', 'q-theme' ),
			'top-center'    => esc_html__( 'Top Center', 'q-theme' ),
			'top-right'     => esc_html__( 'Top Right', 'q-theme' ),
			'center-left'   => esc_html__( 'Center Left', 'q-theme' ),
			'center-center' => esc_html__( 'Center Center', 'q-theme' ),
			'center-right'  => esc_html__( 'Center Right', 'q-theme' ),
			'bottom-left'   => esc_html__( 'Bottom Left', 'q-theme' ),
			'bottom-center' => esc_html__( 'Bottom Center', 'q-theme' ),
			'bottom-right'  => esc_html__( 'Bottom Right', 'q-theme' ),
		],
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'never',
				'operator' => '!==',
			],
		],
		'transport'       => 'postMessage',
		'partial_refresh' => [
			"q_theme_grid_nav_{$id}_expand_icon_position_template" => [
				'selector'            => ".q-tp-nav_{$id}",
				'container_inclusive' => true,
				'render_callback'     => function() use ( $id ) {
					/**
					 * We use include( get_theme_file_path() ) here 
					 * because we need to pass the $id var to the template.
					 */
					include get_theme_file_path( 'grid-parts/navigation/template.php' );
				},
			],
		],
	] );

	q_theme_add_customizer_field( [
		'type'            => 'slider',
		'settings'        => "q_theme_grid_nav_{$id}_collapse_icon_size",
		'label'           => esc_attr__( 'Collapse Icon Size', 'q-theme' ),
		'section'         => "q_theme_grid_part_details_nav_$id",
		'default'         => 1,
		'transport'       => 'postMessage',
		'choices'         => [
			'min'    => .75,
			'max'    => 3,
			'step'   => .01,
			'suffix' => 'em',
		],
		'css_vars'        => [ "--q-nav-$id-collapsed-icon-size", '$em' ],
		'active_callback' => [
			[
				'setting'  => "q_theme_grid_nav_{$id}_collapse_to_icon",
				'value'    => 'never',
				'operator' => '!==',
			],
		],
	] );

	if ( class_exists( 'WooCommerce' ) ) {
		q_theme_add_customizer_field( [
			'type'     => 'custom',
			'settings' => "q_theme_grid_nav_{$id}_woo_title_helper",
			'section'  => "q_theme_grid_part_details_nav_$id",
			'default'  => '<h3 class="q-customizer-section-title">' . esc_html__( 'WooCommerce Options', 'q-theme' ) . '</h3>',
		] );
	
		q_theme_add_customizer_field( [
			'type'            => 'switch',
			'settings'        => "q_theme_grid_nav_{$id}_woo_cart",
			'label'           => esc_attr__( 'Show WooCommerce Cart', 'q-theme' ),
			'description'     => __( 'If enabled, the cart will be added as a dropdown at the end of the menu.', 'q-theme' ),
			'section'         => "q_theme_grid_part_details_nav_$id",
			'default'         => 1 === $id,
			'transport'       => 'auto',
			'transport'       => 'postMessage',
			'partial_refresh' => [
				"q_theme_grid_nav_{$id}_woo_cart_template" => [
					'selector'            => ".q-tp-nav_{$id}",
					'container_inclusive' => true,
					'render_callback'     => function() use ( $id ) {
						/**
						 * We use include( get_theme_file_path() ) here 
						 * because we need to pass the $id var to the template.
						 */
						include get_theme_file_path( 'grid-parts/navigation/template.php' );
					},
				],
			],
		] );
	}
}
