<?xml version="1.0"?>
<ruleset name="WordPress Theme Coding Standards">
	<!-- See https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-ruleset.xml -->
	<!-- See https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards -->
	<!-- See https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/wiki -->
	<!-- See https://github.com/wimg/PHPCompatibility -->

	<exclude-pattern>node_modules</exclude-pattern>
	<exclude-pattern>vendor</exclude-pattern>
	<exclude-pattern>inc/merlin</exclude-pattern>
	<exclude-pattern>bin</exclude-pattern>
	<exclude-pattern>tests/</exclude-pattern>
	<exclude-pattern>inc/class-tgm-plugin-activation.php</exclude-pattern>
	<exclude-pattern>grid-parts/breadcrumbs/hybrid-breadcrumbs</exclude-pattern>
	<exclude-pattern>inc/theme-updater-admin.php</exclude-pattern>

	<!-- Set a description for this ruleset. -->
	<description>A custom set of code standard rules to check for WordPress themes.</description>

	<!-- Pass some flags to PHPCS:
		 p flag: Show progress of the run.
		 s flag: Show sniff codes in all reports.
		 v flag: Print verbose output.
		 n flag: Do not print warnings.
	-->
	<arg value="psv"/>

	<!-- Only check the PHP, CSS and SCSS files. JS files are checked separately with JSCS and JSHint. -->
	<arg name="extensions" value="php,css,scss/css"/>

	<!-- Check all files in this directory and the directories below it. -->
	<file>.</file>

	<!-- Include the WordPress ruleset. -->
	<rule ref="WordPress">
		<exclude name="WordPress.Files.FileName.InvalidClassFileName" />
		<exclude name="WordPress.Files.FileName.NotHyphenatedLowercase" />
		<exclude name="WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet" />
		<exclude name="PEAR.Functions.FunctionCallSignature.ContentAfterOpenBracket" />
		<exclude name="PEAR.Functions.FunctionCallSignature.MultipleArguments" />
		<exclude name="PEAR.Functions.FunctionCallSignature.CloseBracketLine" />
		<exclude name="PHPCompatibility.Syntax.NewShortArray.Found" />
		<exclude name="PHPCompatibility.FunctionDeclarations.NewClosure.Found" />
		<exclude name="PHPCompatibility.LanguageConstructs.NewLanguageConstructs.t_ns_separatorFound" />
		<exclude name="PHPCompatibility.Keywords.NewKeywords.t_useFound" />
		<exclude name="PHPCompatibility.Keywords.NewKeywords.t_dirFound" />
		<exclude name="PHPCompatibility.Keywords.NewKeywords.t_namespaceFound" />
		<exclude name="PHPCompatibility.Syntax.NewDynamicAccessToStatic.Found" />
		<exclude name="WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound" />
		<exclude name="Squiz.PHP.CommentedOutCode.Found" />
		<exclude name="WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents" />
		<exclude name="PHPCompatibility.Syntax.NewFunctionArrayDereferencing.Found" />
	</rule>
	<rule ref="Generic.Arrays.DisallowLongArraySyntax.Found" />
	<rule ref="WPThemeReview" />

	<rule ref="WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet">
		<exclude-pattern>inc/Scripts.php</exclude-pattern>
		<exclude-pattern>inc/Grid.php</exclude-pattern>
	</rule>

	<!-- Verify that the text_domain is set to the desired text-domain.
		 Multiple valid text domains can be provided as a comma-delimited list. -->
	<rule ref="WordPress.WP.I18n">
		<properties>
			<property name="text_domain" type="array" value="q-theme" />
		</properties>
	</rule>

	<!-- Allow for theme specific exceptions to the file name rules based
		 on the theme hierarchy. -->
	<rule ref="WordPress.Files.FileName">
		<properties>
			<property name="is_theme" value="true" />
		</properties>
	</rule>

	<!-- Verify that no WP functions are used which are deprecated or have been removed.
		 The minimum version set here should be in line with the minimum WP version
		 as set in the "Requires at least" tag in the readme.txt file. -->
	<rule ref="WordPress.WP.DeprecatedFunctions">
		<properties>
			<property name="minimum_supported_version" value="4.0" />
		</properties>
	</rule>

	<!-- Include sniffs for PHP cross-version compatibility. -->
	<config name="testVersion" value="7"/>
	<rule ref="PHPCompatibility"/>

	<rule ref="WordPress.Security.EscapeOutput.OutputNotEscaped">
		<exclude-pattern>inc/grid/grid-styles.php</exclude-pattern>
	</rule>
</ruleset>
