<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Q Theme
 */

use Q_Theme\Grid;
use Q_Theme\Grid_Parts;
use Q_Theme\AMP;
?>

		</main>
	</div>
	<?php
	/**
	 * Add grid parts below the content.
	 */
	$active_parts     = Grid_Parts::get_instance()->get_active();
	$content_position = array_search( 'content', $active_parts, true );
	if ( false === $content_position ) {
		return;
	}
	foreach ( $active_parts as $key => $val ) {
		if ( $key > $content_position ) {
			do_action( 'q_theme_the_grid_part', $val );
		}
	}
	?>
</div>

<?php wp_footer(); ?>
</body>
</html>
