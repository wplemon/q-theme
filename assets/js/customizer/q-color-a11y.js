/* global kirki */
wp.customize.controlConstructor['q-color-a11y-checker'] = wp.customize.Control.extend({

	/**
	 * Triggered when the control is ready.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	ready: function() {
		var control = this;

		this.checkScores = _.debounce( _.bind( this.showScores, this ), 100 );

		// Set initial colors.
		setTimeout( function() {
			control.setColors();

			// Show scores.
			control.showScores();
		}, 2000 );

		// Watch for changes.
		control.watchSettings();
	},

	showScores: function() {
		var control = this,
			score   = control.getContrast(),
			html    = '';

		_.each( control.params.choices.rules, function( rule ) {
			var text;
			if ( score >= rule.min && score < rule.max ) {

				// Replace placeholders with the score.
				text = rule.text.replace( '%s', score ).replace( '%s', score ).replace( '%s', score ).replace( '%s', score );

				// Build the HTML.
				html = '<div class="q-a11y-color-wrapper notice notice-' + rule.result + '">' + text + '</div>';
			}
		});

		control.container.html( html );
	},

	/**
	 * Watch defined controls and re-trigger results calculations when there's a change.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	watchSettings: function() {
		var control = this;

		_.each( control.params.choices.colors, function( choice ) {
			if ( 'setting' === choice[0] ) {
				wp.customize( choice[1], function( setting ) {
					setting.bind( function() {

						// Update the colors in the object.
						control.setColors();

						// Retriggers calcs and show new scores.
						control.checkScores();
					});
				});

				if ( -1 < choice[1].indexOf( '[' ) ) {
					wp.customize( choice[1].split( '[' )[0], function( setting ) {
						setting.bind( function() {

							// Update the colors in the object.
							control.setColors();

							// Retriggers calcs and show new scores.
							control.checkScores();
						});
					});
				}
			}
		});
	},

	/**
	 * Set the object's colors property.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	setColors: function() {
		var control = this;
		control.colors    = [];
		control.colors[0] = {
			r: jQuery.Color( control.getColor( 0 ) ).red(),
			g: jQuery.Color( control.getColor( 0 ) ).green(),
			b: jQuery.Color( control.getColor( 0 ) ).blue()
		};
		control.colors[1] = {
			r: jQuery.Color( control.getColor( 1 ) ).red(),
			g: jQuery.Color( control.getColor( 1 ) ).green(),
			b: jQuery.Color( control.getColor( 1 ) ).blue()
		};
		control.colors[0].l = control.getRelativeLuminance( control.colors[0].r, control.colors[0].g, control.colors[0].b );
		control.colors[1].l = control.getRelativeLuminance( control.colors[1].r, control.colors[1].g, control.colors[1].b );
	},

	/**
	 * Get contrast between the 2 colors.
	 *
	 * @since 1.0
	 * @returns {float}
	 */
	getContrast: function() {
		var control   = this,
			contrast1 = control.roundFloat( ( control.colors[0].l + 0.05 ) / ( control.colors[1].l + 0.05 ), 2 ),
			contrast2 = control.roundFloat( ( control.colors[1].l + 0.05 ) / ( control.colors[0].l + 0.05 ), 2 );

		return Math.max( contrast1, contrast2 );
	},

	/**
	 * Gets the relative luminance from RGB.
	 * Formula: http://www.w3.org/TR/2008/REC-WCAG20-20081211/#relativeluminancedef
	 *
	 * @since 1.0
	 * @param {int} r - Red (0-255).
	 * @param {int} g - Green (0-255).
	 * @param {int} b - Blue (0-255).
	 * @returns {float}
	 */
	getRelativeLuminance: function( r, g, b ) {

		function qGetLumPart( val ) {
			val = val / 255;
			if ( 0.03928 >= val ) {
				return val / 12.92;
			}
			return Math.pow( ( ( val + 0.055 ) / 1.055 ), 2.4 );
		}
		return 0.2126 * qGetLumPart( r ) + 0.7152 * qGetLumPart( g ) + 0.0722 * qGetLumPart( b );
	},

	/**
	 * Gets a color defined in the control's choices.
	 *
	 * @since 1.0
	 * @param {int} i 0 or 1.
	 * @returns {string}
	 */
	getColor: function( i ) {
		var control = this,
			colors  = control.params.choices.colors;

		// Return the color if hard-coded.
		if ( colors[ i ] && colors[ i ][0] && 'color' === colors[ i ][0] ) {
			return colors[ i ][1];
		}

		// Check if we want to retrieve the value from a setting.
		if ( colors[ i ] && colors[ i ][0] && 'setting' === colors[ i ][0] && colors[ i ][1] ) {
			return kirki.setting.get( colors[ i ][1] );
		}
	},

	/**
	 * Round a float.
	 * It's by no means perfect, but it gets the job done.
	 *
	 * @since 1.0
	 * @param {float} number - The number we want to round.
	 * @returns {float}
	 */
	roundFloat: function( number ) {
		return Math.round( number * 100 ) / 100;
	}
});
