wp.customize.controlConstructor.q_section_help = wp.customize.Control.extend({

	/**
	 * Triggered when the control is ready.
	 *
	 * @since 1.0
	 * @returns {void}
	 */
	ready: function() {
        var control = this,
            content = false,
            http;

        control.container.find( '.q-help-section-link' ).on( 'click', function( e ) {
			e.preventDefault();

			control.container.find( '.q-section-help-wrapper' ).toggleClass( 'active' );
			jQuery( this ).toggleClass( 'active' );

            if ( ! content && ! control.params.choices.content ) {

				http = new XMLHttpRequest();
				http.open( 'GET', control.params.choices.remote_url );
				http.send();
				http.onreadystatechange = function() {
					var contentSuccessful;
					try {
						contentSuccessful = JSON.parse( http.responseText ).content.rendered;
					} catch ( e ) {
						contentSuccessful = false;
					}

					if ( contentSuccessful ) {
						content = contentSuccessful;
					}

					if ( content ) {
						control.container.find( '.q-section-help-wrapper' ).addClass( 'content-ready' );
						control.container.find( '.q-section-help-content' ).html( content );
					}
				};
			}
		});
	}
});
