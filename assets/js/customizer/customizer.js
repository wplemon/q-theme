/* global qTemplatePreviewScript */

/**
 * Scripts within the customizer controls window.
 */
( function() {

	var qSetGridVal = function( setting, value ) {
		var control = wp.customize.control( setting );
		if ( 'q_theme_grid' === control.params.type ) {

			// Put source value to the target setting.
			control.gridVal = jQuery.extend({}, value );

			// Redraw and save new value.
			control.drawGrid();
			control.drawPreview();
			control.save();
		}
	};

	wp.customize.bind( 'ready', function() {

		// Focus buttons.
		jQuery( '.button-q-focus.global-focus' ).click( function( e ) {
			wp.customize[ jQuery( this ).data( 'context' ) ]( jQuery( this ).data( 'focus' ) ).focus();
			e.preventDefault();
		});

		// Copy-grid-settings buttons.
		jQuery( '.button-q-copy-grid-setting' ).click( function( e ) {
			qSetGridVal( jQuery( this ).data( 'to' ), wp.customize.control( jQuery( this ).data( 'from' ) ).gridVal );

			e.preventDefault();
		});

		// Grid Presets.
		_.each( [
			'q_theme_global_grid_preset'
		], function( setting ) {
			var control = wp.customize.control( setting );

			wp.customize( setting, function( value ) {
				value.bind( function( to ) {
					_.each( control.params.preset, function( preset, valueToListen ) {
						if ( valueToListen === to ) {
							_.each( preset.settings, function( controlValue, controlID ) {
								qSetGridVal( controlID, controlValue );
							});
						}
					});
				});
			});
		});

		// Toggle Desktop/mobile viewport changes.
		_.each( [ 'q_theme_global_grid_toggle', 'q_theme_footer_grid_toggle', 'q_theme_header_grid_toggle' ], function( id ) {

			// When the option changes.
			wp.customize( id, function( setting ) {
				setting.bind( function( value ) {
					jQuery( '.wp-full-overlay-footer .devices button.preview-' + value ).click();
				});
			});

			// When the viewport changes.
			jQuery( '.wp-full-overlay-footer .devices button' ).on( 'click', function() {
				var viewport;
				if ( jQuery( this ).hasClass( 'preview-mobile' ) ) {
					viewport = 'mobile';
				} else if ( jQuery( this ).hasClass( 'preview-desktop' ) ) {
					viewport = 'desktop';
				}
				setTimeout( function() {
					var input        = jQuery( '#customize-control-' + id + ' input[value="' + viewport + '"]' ),
						hideControls = [
							'header_image',
							'background_color',
							'background_image',
							'background_position',
							'background_size',
							'background_repeat',
							'background_attachment',
							'background_preset'
						];

					input.click();
					input.prop( 'checked', true );

					// Set the active state of some controls that need to be hidden.
					if ( 'mobile' === viewport ) {
						_.each( hideControls, function( hideControl ) {
							wp.customize.control( hideControl ).deactivate();
						});
					} else {
						_.each( hideControls, function( hideControl ) {
							wp.customize.control( hideControl ).activate();
						});
					}

				}, 100 );
			});
		});

		// Back buttons in nested grids.
		_.each( window.qGridPartsSelectedAreas, function( parts, grid ) {
			var nestedParts = {};

			_.each( qTemplatePreviewScript.nestedGrids, function( v, k ) {
				nestedParts[ k ] = v;
			});

			// Check and make sure we're not in the main grid.
			if ( 'q_theme_grid' !== grid ) {

				// Loop parts in the sub-grid.
				_.each( parts, function( part ) {
					var section = jQuery( '#sub-accordion-section-q_theme_grid_part_details_' + part ),
						backBtn = section.find( '.customize-section-back' );

					// Change the behavior of the back button.
					jQuery( backBtn ).click( function( e ) {
						if ( 'q_theme_header_grid' === grid ) {
							wp.customize.section( 'q_theme_grid_part_details_header' ).focus();
							e.preventDefault();
						} else if ( 'q_theme_footer_grid' === grid ) {
							wp.customize.section( 'q_theme_grid_part_details_footer' ).focus();
							e.preventDefault();
						} else if ( nestedParts[ grid ] && wp.customize.section( 'q_theme_grid_part_details_' + nestedParts[ grid ] ) ) {
							wp.customize.section( 'q_theme_grid_part_details_' + nestedParts[ grid ] ).focus();
							e.preventDefault();
						} else if ( qTemplatePreviewScript.nestedGrids[ grid ] && wp.customize.section( 'q_theme_grid_part_details_' + qTemplatePreviewScript.nestedGrids[ grid ] ) ) {
							wp.customize.section( 'q_theme_grid_part_details_' + qTemplatePreviewScript.nestedGrids[ grid ] ).focus();
							e.preventDefault();
						}
					});
				});
			}
		});
	});
}( jQuery ) );
