<?php
/**
 * Template part for displaying posts
 *
 * @package Q Theme
 * @since 1.0
 */

q_theme_the_edit_link();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
