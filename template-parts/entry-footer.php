<?php
/**
 * Template part for displaying posts
 *
 * @package Q Theme
 * @since 1.0
 */

if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
	echo '<span class="comments-link">';
	q_theme_the_comments_link();
	echo '</span>';
}
q_theme_the_edit_link();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
