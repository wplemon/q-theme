<?php
/**
 * Template part for displaying the post-tags.
 *
 * @package Q Theme
 * @since 1.0
 */

$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'q-theme' ) );
if ( $tags_list ) {
	echo '<span class="tags-links">';
	/* translators: 1: list of tags. */
	printf( esc_html__( 'Tagged %1$s', 'q-theme' ), $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	echo '</span>';
}

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
