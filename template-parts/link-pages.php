<?php
/**
 * Template part for the previous/next links in posts & pages.
 *
 * @package Q Theme
 * @since 1.0
 */

if ( ! get_theme_mod( 'q_theme_show_next_prev', true ) ) {
	return;
}
wp_link_pages( [
	'before'    => '<div class="page-links"><span class="label">' . esc_html__( 'Pages:', 'q-theme' ) . '</span><span class="item">',
	'after'     => '</span></div>',
	'separator' => '</span><span class="item">',
] );

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
