<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Q Theme
 */

use Q_Theme\Grid;
use Q_Theme\Style;
use Q_Theme\Grid_Part\Content;
use Q_Theme\Grid_Parts;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( is_singular() && pings_open() ) : // Add a pingback url auto-discovery header for singularly identifiable articles. ?>
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'q-theme' ); ?></a>
<?php
/**
 * Add the main grid styles.
 *
 * @since 1.0
 */
$style = new Style();
$style->add_string( Grid::get_styles_responsive( [
	'large'      => Grid::get_options( 'q_theme_grid' ),
	'small'      => ( 'custom' === get_theme_mod( 'q_theme_grid_mobile_mode', 'default' ) ) ? Grid::get_options( 'q_theme_grid_mobile' ) : false,
	'breakpoint' => get_theme_mod( 'q_mobile_breakpoint', '800px' ),
	'selector'   => '.q-site-wrapper',
	'prefix'     => false,
] ) );
?>
<div id="page" class="site q-site-wrapper">
	<?php
	/**
	 * Print styles.
	 */
	$style->the_css( 'q-inline-css-main-grid' );
	?>
	<?php
	/**
	 * Add grid parts above the content.
	 */
	$active_parts     = Grid_Parts::get_instance()->get_active();
	$content_position = array_search( 'content', $active_parts, true );
	if ( false === $content_position ) {
		return;
	}
	foreach ( $active_parts as $key => $val ) {
		if ( $key < $content_position ) {
			do_action( 'q_theme_the_grid_part', $val );
		}
	}
	?>
	<div id="content" class="site-content q-tp q-tp-content">
		<?php
		/**
		 * Add styles for the content.
		 *
		 * @since 1.0
		 */
		Content::print_styles();
		?>
		<main id="main" class="site-main inner">
